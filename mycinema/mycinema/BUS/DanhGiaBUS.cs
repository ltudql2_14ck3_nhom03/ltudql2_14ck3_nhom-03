﻿using mycinema.Models;
using mycinema.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class DanhGiaBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public void DanhGia(DanhGia danhGia)
        {
            DanhGia danhGiaCheck = db.DanhGias.Where(d => d.MaNguoiDung.CompareTo(danhGia.MaNguoiDung) == 0 &&
            d.MaPhim.CompareTo(danhGia.MaPhim) == 0).SingleOrDefault();
            bool existed = danhGiaCheck == null ? false : true;
            if (existed)
            {
                DanhGia danhGiaGet = db.DanhGias.Where(d => d.MaNguoiDung.CompareTo(danhGia.MaNguoiDung) == 0 &&
                    d.MaPhim.CompareTo(danhGia.MaPhim) == 0).Single();
                danhGiaGet.SoSao = danhGia.SoSao;
                db.SaveChanges();
            }
            else
            {
                db.DanhGias.Add(danhGia);
                db.SaveChanges();
            }
        }

        public int laySoSaoCuaNguoiDanhGia(string maNguoiDung, string maPhim)
        {
            DanhGia danhGiaCheck = db.DanhGias.Where(d => d.MaNguoiDung.CompareTo(maNguoiDung) == 0 &&
            d.MaPhim.CompareTo(maPhim) == 0).SingleOrDefault();
            bool existed = danhGiaCheck == null ? false : true;
            int soSao = 0;
            if (existed)
            {
                soSao = int.Parse(danhGiaCheck.SoSao.ToString());
            }
                
            return soSao;
        }

        public DuLieuDanhGiaPhim layDuLieuDanhGiaPhim(string maPhim)
        {
            double? avgRating;
            int roundStar;
            List<DanhGia> danhGia = db.DanhGias.Where(d => d.MaPhim.CompareTo(maPhim) == 0).ToList();
            avgRating = danhGia.Average(d => d.SoSao) as double?;
            if (avgRating == null)
                roundStar = 0;
            else
                roundStar = (int)avgRating;
            int soNguoiDanhGia = danhGia.Count;
            return new DuLieuDanhGiaPhim() { SoSao = roundStar, SoNguoiDanhGia = soNguoiDanhGia };
        }


    }
}
