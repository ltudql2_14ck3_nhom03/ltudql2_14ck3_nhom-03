﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class PhongBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public PhongChieu layThongTinPhong(Ghe g)
        {
            return db.PhongChieux.Where(r => r.MaPhong.Equals(g.MaPhong)).SingleOrDefault();
        }
    }
}
