﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class KhachHangBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public int DoiVePhim(string _maKhachHang)
        {
            var kh = db.KhachHangs.Where(k => k.MaKhachHang == _maKhachHang).Single();
            kh.SoVeDoiThuong = kh.DiemThuong / 10;
            kh.DiemThuong = kh.DiemThuong % 10;
            int n = db.SaveChanges();
            if (n > 0)
            {
                return (int) kh.SoVeDoiThuong;
            }
            return 0;
        }
    }
}
