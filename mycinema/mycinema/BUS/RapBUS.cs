﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class RapBUS
    {
        private  QLRapChieuEntities db = new QLRapChieuEntities();
        public  List<Rap> layDanhSachToanBoRap(){
            List<Rap> listRap = (List<Rap>) db.Raps.Select(p => p).ToList<Rap>();
            return listRap;
        }

        public Rap layThongTinRap(SuatChieu sc)
        {
            PhongChieu pc = db.PhongChieux.Where(p => p.MaPhong.Equals(sc.MaPhong)).Single();
            Rap r = db.Raps.Where(c => c.MaRap.Equals(pc.MaRap)).Single();
            return r;

        }

        public List<Phim> layDanhPhimCuaRap(string maRap)
        {
            List<PhongChieu> listPhong = (List<PhongChieu>)db.PhongChieux.Where(p => p.MaRap.CompareTo(maRap) == 0).ToList();
            List<SuatChieu> listSuatChieu = (List<SuatChieu>)db.SuatChieux.Select(p => p).ToList();
            List<Phim> listPhim = (List<Phim>)db.Phims.Select(ph => ph).ToList();
            List<Phim> listRst = (from sc in listSuatChieu
                                        join p in listPhong on sc.MaPhong equals p.MaPhong
                                        join ph in listPhim on sc.MaPhim equals ph.MaPhim
                                        select ph
                                        ).Distinct().ToList<Phim>();
            return listRst;
        }

        public void themRap(Rap rap)
        {
            db.Raps.Add(rap);
            db.SaveChanges();

        }

    }
}
