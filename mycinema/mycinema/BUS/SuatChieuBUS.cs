﻿using mycinema.Booking;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    
    public class SuatChieuBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public List<SuatChieu> layDanhSachSuatChieu(string maRap, string maPhim)
        {
            DateTime startDt = DateTime.Parse("2017-05-06");
            DateTime endDt = startDt.AddDays(2);
            List<Rap> lstrap = db.Raps.Where(r => r.MaRap.Equals(maRap)).Select(r => r).ToList<Rap>();
            foreach(var rap in lstrap)
            {
                List<PhongChieu> lstPhong = rap.PhongChieux.Select(p => p).ToList<PhongChieu>();

                List<SuatChieu> lstSuat = new List<SuatChieu>();

                foreach (var phong in lstPhong)
                {
                    List<SuatChieu> lsSuatSub = phong.SuatChieux.Where(s => s.MaPhim.Equals(maPhim) && s.NgayChieu >= startDt && s.NgayChieu <= endDt).OrderBy(s => s.NgayChieu).Select(s => s).ToList<SuatChieu>();
                    if(lsSuatSub.Count > 0)
                    {
                        lstSuat.AddRange(lsSuatSub);
                    }
                    
                }

                return lstSuat;
            }
            return null;
        }

        public SuatChieu layThongTinSuatChieu(string masuat)
        {
            return db.SuatChieux.Where(s => s.MaSuatChieu.Equals(masuat)).SingleOrDefault();
            
        }

        public Rap layThongTinRap(SuatChieu x)
        {
            string maR = db.PhongChieux.Where(p => p.MaPhong.Equals(x.MaPhong)).Select(p => p.MaRap).Distinct().First();
            Rap rap = db.Raps.Where(r => r.MaRap.Equals(maR)).Select(r => r).Distinct().First();

            return rap;
        }

        public List<DanhSachNgayChieu> layLichChieu(string maRap, string maPhim)
        {

            DateTime firstD = DateTime.Parse("2017-05-06");
            DateTime secondD = firstD.AddDays(1);
            DateTime thirdD = firstD.AddDays(2);


            List<SuatChieu> all = layDanhSachSuatChieu(maRap, maPhim);

            List<SuatChieu> firstSC = new List<SuatChieu>();
            List<SuatChieu> secondSC = new List<SuatChieu>();
            List<SuatChieu> thirdSC = new List<SuatChieu>();

            

            foreach (SuatChieu sc in all)
            {
                if (sc.NgayChieu.Equals(firstD))
                {
                    firstSC.Add(sc);
                }
                if (sc.NgayChieu.Equals(secondD))
                {
                    secondSC.Add(sc);
                }
                if (sc.NgayChieu.Equals(thirdD))
                {
                    thirdSC.Add(sc);
                }
            }

            List<DanhSachNgayChieu> ds = new List<DanhSachNgayChieu>()
            {
                new DanhSachNgayChieu
                {
                    ngay = firstD,
                    lstSuat = new ObservableCollection<SuatChieu>(firstSC)

                },

                new DanhSachNgayChieu
                {
                    ngay = secondD,
                    lstSuat = new ObservableCollection<SuatChieu>(secondSC)

                },

                new DanhSachNgayChieu
                {
                    ngay = thirdD,
                    lstSuat = new ObservableCollection<SuatChieu>(thirdSC)

                },

            };

            return ds;
        }
        public List<DateTime?> layNgayChieuTheoRap(string _maRap)
        {
            return db.SuatChieux.Where(sc => sc.PhongChieu.MaRap == _maRap).Where(sc => DateTime.Compare((DateTime)sc.NgayChieu,DateTime.Today) >= 0).OrderByDescending(sc => sc.NgayChieu).Select(sc => sc.NgayChieu).Distinct().ToList();
        }
    }
}
