﻿using mycinema.Booking;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class GheBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public List<Ghe> layToanBoGhe(string maPhong)
        {

    
            
            List<Ghe> lstGhe = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g).ToList<Ghe>();
            return lstGhe;
        }

        public List<Ghe> layCacGheDaDat(string maPhong,string maSuat)
        {
            List<Ghe> lstGheDaDat = db.Ghes.Join(db.VeXemPhims, g => g.MaGhe, v => v.MaGhe, (g, v) => new { g = g, v = v })
                .Where(i => (i.g.MaPhong.Equals(maPhong)) && (i.v.MaSuatChieu.Equals(maSuat))).Select(i => i.g).ToList<Ghe>();
            return lstGheDaDat;
        }

        public List<String> laySoHangGhe(string maPhong)
        {
            List<String> lstHang = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g.DayGhe).Distinct().ToList<String>();
            return lstHang;
        }

        public List<int?> laySoCotGhe(string maPhong)
        {
            List<int?> lstCot = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g.SoTT).Distinct().ToList<int?>();
            return lstCot;
        }


        public Ghe LayThongTinGhe(VeXemPhim vxp)
        {
            return db.Ghes.Where(c => c.MaGhe.Equals(vxp.MaGhe)).SingleOrDefault();
        }

        public DanhSachThongTinGhe layDanhSachGhe(SuatChieu sc)
        {

            string maPhong = sc.MaPhong;
            string maSuat = sc.MaSuatChieu;
            List<Ghe> tongGhe = layToanBoGhe(maPhong);
            List<Ghe> cacGheDaDat = layCacGheDaDat(maPhong, maSuat);

            List<ThongTinGhe> dsThongtin = new List<ThongTinGhe>();

            for(int i = 0; i < tongGhe.Count; i++)
            {
                ThongTinGhe tt = null;
                if (cacGheDaDat.Contains(tongGhe.ElementAt(i)))
                {
                    tt = new ThongTinGhe()
                    {
                        ghe = tongGhe.ElementAt(i),
                        tinhTrang = 1,
                    };
                }
                else
                {
                    tt = new ThongTinGhe()
                    {
                        ghe = tongGhe.ElementAt(i),
                        tinhTrang = 0,
                    };
                }
                dsThongtin.Add(tt);

            }

            DanhSachThongTinGhe ds = new DanhSachThongTinGhe()
            {
                lstSoCot = new ObservableCollection<int?>(laySoCotGhe(maPhong)),
                lstSoHang = new ObservableCollection<string>(laySoHangGhe(maPhong)),
                lstGhe = new ObservableCollection<ThongTinGhe>(dsThongtin)
            };

            
        
            return ds;
        }
    }
}
