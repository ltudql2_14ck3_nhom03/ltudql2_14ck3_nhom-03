﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class PhimBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public List<Phim> timKiemPhimTheoTen(string _tenphim)
        {
            List<Phim> ls = db.SuatChieux.Where(s => s.Phim.TenPhim.Contains(_tenphim)).Where(s => s.NgayChieu >= DateTime.Today).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        public List<Phim> timKiemPhimTheoTen1(string _tenphim, string _maRap,string _maQuocGia, string _maTheLoai, DateTime _ngayChieu)
        {
            var ls = db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.Phim.TheLoai == _maTheLoai).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen2(string _tenphim, string _maRap, DateTime _ngayChieu, string _maTheLoai)
        {
            var ls = db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Where(s => s.Phim.TheLoai == _maTheLoai).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen3(string _tenphim, string _maRap, DateTime _ngayChieu, string _maQuocGia)
        {
            var ls = db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen4(string _tenphim,string _maQuocGia, string _maTheLoai)
        {
            var ls = db.SuatChieux.Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.Phim.TheLoai == _maTheLoai).Where(s => s.NgayChieu >= DateTime.Today).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen5(string _tenphim, string _maRap, DateTime _ngayChieu)
        {
            var ls = db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen6(string _tenphim, string _maTheLoai)
        {
            var ls = db.SuatChieux.Where(s => s.Phim.TheLoai == _maTheLoai).Where(s => s.NgayChieu >= DateTime.Today).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> timKiemPhimTheoTen7(string _tenphim, string _maQuocGia)
        {
            var ls = db.SuatChieux.Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.NgayChieu >= DateTime.Today).Where(s => s.Phim.TenPhim.Contains(_tenphim)).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> layToanBoPhim()
        {
            return db.SuatChieux.Select(s => s.Phim).Distinct().ToList();
        }


        public Phim layThongTinBoPhim(SuatChieu sc)
        {
            return db.Phims.Where(m => m.MaPhim.Equals(sc.MaPhim)).SingleOrDefault();

        }
        //OK
        public List<Phim> locPhimTheoTatCaDieuKien(string _maRap, DateTime _ngayChieu, string _maTheLoai, string _maQuocGia)
        {
            var ls = db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.Phim.TheLoai == _maTheLoai).Select(s => s.Phim).Distinct().ToList();
            return ls;
        }
        //OK
        public List<Phim> locPhimTheoRap_TheLoai(string _maRap, string _maTheLoai, DateTime ngaychieu)
        {
            return db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == ngaychieu).Where(s => s.Phim.TheLoai == _maTheLoai).Select(s => s.Phim).Distinct().ToList();
        }
        //OK
        public List<Phim> locPhimTheoRap_QG(string _maRap, string _maQuocGia, DateTime ngaychieu)
        {
            return db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == ngaychieu).Where(s => s.Phim.QuocGia == _maQuocGia).Select(s => s.Phim).Distinct().ToList();
        }
        //OK
        public List<Phim> locPhimTheoQG_TL(string _maQuocGia, string _maTheLoai)
        {
            return db.SuatChieux.Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.Phim.TheLoai == _maTheLoai).Where(s => s.NgayChieu >= DateTime.Today).Select(s => s.Phim).Distinct().ToList();
        }
        //OK
        public List<Phim> locPhimTheoRap(string _maRap, DateTime _ngayChieu)
        {
            return db.SuatChieux.Where(s => s.PhongChieu.MaRap == _maRap).Where(s => s.NgayChieu == _ngayChieu).Select(s => s.Phim).Distinct().ToList();
        }
        //OK
        public List<Phim> locPhimTheoTheLoai(string _theLoai)
        {
            return db.SuatChieux.Where(s => s.Phim.TheLoai == _theLoai).Where(s => s.NgayChieu >= DateTime.Today).Select(s => s.Phim).Distinct().ToList();
        }
        //OK
        public List<Phim> locPhimTheoQuocGia(string _maQuocGia)
        {
            return db.SuatChieux.Where(s => s.Phim.QuocGia == _maQuocGia).Where(s => s.NgayChieu >= DateTime.Today).Select(s => s.Phim).Distinct().ToList();
        }

        public Phim layPhimTheoMaPhim(string MaPhim)
        {
            return db.Phims.Where(p => p.MaPhim.CompareTo(MaPhim) == 0).Single();
        }
    }
}
