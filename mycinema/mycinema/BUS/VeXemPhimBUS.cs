﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class VeXemPhimBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public void luuThongTinVe(string maSc, string maGhe, string mangd, List<VeXemPhim> lstVe)
        {
            
            DateTime date = DateTime.Now;
            string year = date.Year.ToString();
            string month = date.Month.ToString();
            string day = date.Day.ToString();
            string hour = date.Hour.ToString();
            string second = date.Second.ToString();
            string maVe = year + month + day + hour + second;
            VeXemPhim vxp = new VeXemPhim()
            {
                MaVe = maVe,
                MaSuatChieu = maSc,
                MaGhe = maGhe,
                MaNguoiDung = mangd,
                ThoiGianDat = date,
                GiaVe = 60000
            };
            lstVe.Add(vxp);

            db.VeXemPhims.Add(vxp);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //return 0;
            }

            
        }
        public List<VeXemPhim> layVeDat(string _maNguoiDung)
        {
            var ls = db.VeXemPhims.Where(v => v.MaNguoiDung == _maNguoiDung).Where(v => v.SuatChieu.NgayChieu >= DateTime.Today).OrderBy(v => v.SuatChieu.NgayChieu).ToList();
            return ls;
        }
    }
}
