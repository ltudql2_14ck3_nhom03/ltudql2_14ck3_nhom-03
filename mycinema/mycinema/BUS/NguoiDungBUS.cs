﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class NguoiDungBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public NguoiDung layThongTinNguoiDung(string ma)
        {
            var nd = db.NguoiDungs.Where(n => n.MaNguoiDung == ma).Select(n => n).Single();
            return nd;
        }
        public int CapNhatThongTin(string _maNguoiDung, string _hoTen, string _SDT, string _CMND, DateTime _ngaySinh)
        {
            var nd = db.NguoiDungs.Where(nn => nn.MaNguoiDung == _maNguoiDung).Single() as NguoiDung;
            nd.TenNguoiDung = _hoTen;
            nd.Sdt = _SDT;
            nd.Cmnd = _CMND;
            nd.NgaySinh = _ngaySinh;
            int n = db.SaveChanges();
            if (n > 0)
            {
                return n;
            }
            return 0;
        }
        public void capNhatThongTinNguoiDung(string makh, long tongTien) {
            KhachHang kh = db.KhachHangs.Where(c => c.MaKhachHang.Equals(makh)).Select(c => c).First();
            var value = db.KhachHangs.Find(makh);
            if(value != null)
            {
                var diemCu = value.DiemThuong;
                value.DiemThuong = (int)(tongTien / 60000) * 100 + diemCu;


                try
                {
                    db.SaveChanges();

                }catch(Exception ex)
                {
                    //return 0;
                }
            }

            
        }

        public bool NhapDungMatKhau(string _maNguoiDung, string _matKhau)
        {
            var nd = db.NguoiDungs.Where(n => n.MaNguoiDung == _maNguoiDung).Single();
            if(nd.Password == _matKhau)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool DoiMatKhau(string _maNguoiDung, string _matKhauMoi)
        {
            var nd = db.NguoiDungs.Where(nn => nn.MaNguoiDung == _maNguoiDung).Single();
            nd.Password = _matKhauMoi;
            int n = db.SaveChanges();
            if(n >0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
