﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace mycinema.Converters
{
    class StatusConverter : IValueConverter
    {
        private const string ACTIVE_STATUS = "Kích hoạt";
        private const string DISABLE_STATUS = "Khóa";
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = 0;
            string status = value.ToString().Trim();
            if (status != null && status.Length > 0)
            {
                if (status.CompareTo(ACTIVE_STATUS) == 0)
                    index = 0;
                else if (status.CompareTo(DISABLE_STATUS) == 0)
                    index = 1;
                else
                    throw new ArgumentException();
            }
            else
            {
                throw new ArgumentException();
            }
            return index;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int index = 0;
            bool parsed = int.TryParse(value.ToString(), out index);
            string cb = null;
            if (parsed)
            {
                if (index == 0)
                {
                    cb = ACTIVE_STATUS;
                }
                else if (index == 1)
                {
                    cb = DISABLE_STATUS;
                }
            }
            if (cb == null)
                throw new ArgumentException();
            return cb;
        }
    }
}
