﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.ThanhVien
{
    /// <summary>
    /// Interaction logic for ThongTinTaiKhoan.xaml
    /// </summary>
    public partial class ThongTinTaiKhoan : UserControl
    {
        public static MainWindow mainWindow;

        public ThongTinTaiKhoan(MainWindow windows, string _manguoidung)
        {
            mainWindow = windows;
            InitializeComponent();
            this.DataContext = new NguoiDungBUS().layThongTinNguoiDung(_manguoidung);
        }

        private void btnDoiMatKhau_Click(object sender, RoutedEventArgs e)
        {
            Window wd = new DoiMatKhau(tbMaNguoiDung.Text);
            wd.ShowDialog();
        }

        private void btnCapNhatThongTin_Click(object sender, RoutedEventArgs e)
        {
            DateTime temp;
            if (!String.IsNullOrWhiteSpace(txtHoTen.Text) && !Validation.GetHasError(txtHoTen)
                    && !String.IsNullOrWhiteSpace(txtSDT.Text) && !Validation.GetHasError(txtSDT)
                    && !String.IsNullOrWhiteSpace(txtCMND.Text) && !Validation.GetHasError(txtCMND)
                    && !String.IsNullOrWhiteSpace(txtNgaySinh.Text) && DateTime.TryParse(txtNgaySinh.Text, out temp))
            {
                if (MessageBox.Show("Quý khách có thật sự muốn cập nhật", "Xác nhận", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    int n = new NguoiDungBUS().CapNhatThongTin(tbMaNguoiDung.Text, txtHoTen.Text, txtSDT.Text, txtCMND.Text, DateTime.Parse(txtNgaySinh.Text));
                    if (n > 0)
                    {
                        chinhsuathongtin.IsChecked = false;
                        var messageQueue = Snackbar.MessageQueue;
                        string message = "Cập nhật thành công";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);
                    }
                    else
                    {
                        var messageQueue = Snackbar.MessageQueue;
                        string message = "Cập nhật thất bại";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);
                    }
                }
            }
            else if(String.IsNullOrWhiteSpace(txtHoTen.Text) || String.IsNullOrWhiteSpace(txtSDT.Text) || String.IsNullOrWhiteSpace(txtCMND.Text) || String.IsNullOrWhiteSpace(txtNgaySinh.Text))
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Vui lòng nhập đầy đủ thông tin để có thể cập nhật";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
                return;
            }
            else if (Validation.GetHasError(txtSDT))
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Số điện thoại không hợp lệ";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
                return;
            }
            else if (Validation.GetHasError(txtCMND))
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Số CMND không hợp lệ";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
                return;
            }
            else if (!DateTime.TryParse(txtNgaySinh.Text, out temp))
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Ngày sinh không hợp lệ";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
            }            
        }

        private void btnDoiVe_Click(object sender, RoutedEventArgs e)
        {
            if(MessageBox.Show("Quý khách có thật sự muốn đổi vé phim ?", "Xác nhận", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                int n = new KhachHangBUS().DoiVePhim(tbMaNguoiDung.Text);
                if(n > 0)
                {
                    this.DataContext = new NguoiDungBUS().layThongTinNguoiDung(tbMaNguoiDung.Text);
                    var messageQueue = Snackbar.MessageQueue;
                    string message = String.Format("Bạn đã đổi được {0} vé miễn phí từ điểm thưởng",n);
                    Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
                }
                else
                {
                    var messageQueue = Snackbar.MessageQueue;
                    string message = "Đổi vé thất bại. Hãy tích lũy được 10 điểm để đổi 1 vé";
                    Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
                }
                
            }
        }
    }
}
