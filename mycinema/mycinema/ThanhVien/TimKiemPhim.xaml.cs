﻿using mycinema.Booking;
using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.ThanhVien
{
    /// <summary>
    /// Interaction logic for VePhimDaDat.xaml
    /// </summary>
    public partial class TimKiemPhim : UserControl
    {
        public static MainWindow mainWindow;
        public int loaiPhepLoc = 0;
        public TimKiemPhim(MainWindow windows)
        {
            mainWindow = windows;
            InitializeComponent();
            cmbRap.ItemsSource = new RapBUS().layDanhSachToanBoRap();
            
            cmbTheLoai.ItemsSource = new TheLoaiBUS().layToanBoTheLoai(); ;
            
            
            cmbQuocGia.ItemsSource = new QuocGiaBUS().layToanBoQuocGia();
            if(cmbRap.SelectedValue != null)
            {
                cmbNgayChieu.ItemsSource = new SuatChieuBUS().layNgayChieuTheoRap(cmbRap.SelectedValue.ToString());
            }
           listP.ItemsSource = new PhimBUS().layToanBoPhim();
        }

        public TimKiemPhim()
        {
            InitializeComponent();
        }

        private void RepeatButton_Click(object sender, RoutedEventArgs e)
        {
            InitializeComponent();
            if(loaiPhepLoc == 0)
            {
                if (txtTenPhim.Text == "")
                {
                    listP.ItemsSource = new PhimBUS().layToanBoPhim();
                }
                else
                {
                    listP.ItemsSource = new PhimBUS().timKiemPhimTheoTen(txtTenPhim.Text);
                }
            }
            Rap rap = null;
            if (cmbRap.SelectedItem != null)
            {
                rap = cmbRap.SelectedItem as Rap;
            }
            DateTime ngaychieu = new DateTime(2000, 1, 1);
            if (cmbNgayChieu.SelectedItem != null)
            {
                ngaychieu = (DateTime)cmbNgayChieu.SelectedItem;
            }
            TheLoai theloai = null;
            if (cmbTheLoai.SelectedItem != null)
            {
                theloai = cmbTheLoai.SelectedItem as TheLoai;
            }
            QuocGia quocgia = null;
            if (cmbQuocGia.SelectedItem != null)
            {
                quocgia = cmbQuocGia.SelectedItem as QuocGia;
            }
            if (loaiPhepLoc == 1)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen1(txtTenPhim.Text, rap.MaRap, quocgia.MaQuocGia, theloai.MaTheLoai, ngaychieu);
                listP.ItemsSource = ls;
            }
            if (loaiPhepLoc == 2)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen2(txtTenPhim.Text, rap.MaRap, ngaychieu, theloai.MaTheLoai);
                listP.ItemsSource = ls;
            }
            if (loaiPhepLoc == 3)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen3(txtTenPhim.Text, rap.MaRap, ngaychieu, quocgia.MaQuocGia);
                listP.ItemsSource = ls;
            }
            if (loaiPhepLoc == 4)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen4(txtTenPhim.Text, quocgia.MaQuocGia, theloai.MaTheLoai);
                listP.ItemsSource = ls;
            }
            if (loaiPhepLoc == 5)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen5(txtTenPhim.Text, rap.MaRap, ngaychieu);
                listP.ItemsSource = ls;
            }
            if (loaiPhepLoc == 6)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen6(txtTenPhim.Text, theloai.MaTheLoai);
                listP.ItemsSource = ls.Where(p => p.TheLoai == theloai.MaTheLoai).ToList();
            }
            if (loaiPhepLoc == 7)
            {
                var ls = new PhimBUS().timKiemPhimTheoTen7(txtTenPhim.Text, quocgia.MaQuocGia);
                listP.ItemsSource = ls;
            }
        }

        private void cmbRap_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Rap d1 = cmbRap.SelectedItem as Rap;
            if (d1.TenRap != "")
            {
                cmbNgayChieu.ItemsSource = new SuatChieuBUS().layNgayChieuTheoRap(d1.MaRap);
            }
        }

        private void btnLocPhim_Click(object sender, RoutedEventArgs e)
        {
            Rap rap = null;
            if (cmbRap.SelectedItem != null)
            {
                rap = cmbRap.SelectedItem as Rap;
            }
            DateTime ngaychieu = new DateTime(2000,1,1);
            if (cmbNgayChieu.SelectedItem != null)
            {
                ngaychieu = (DateTime)cmbNgayChieu.SelectedItem;
            }
            TheLoai theloai = null;
            if(cmbTheLoai.SelectedItem != null)
            {
                theloai = cmbTheLoai.SelectedItem as TheLoai;
            }
            QuocGia quocgia = null;
            if(cmbQuocGia.SelectedItem != null)
            {
                quocgia = cmbQuocGia.SelectedItem as QuocGia;
            }
            //bat dau loc
            //tim theo tat ca
            if (rap != null && ngaychieu.Date.ToString() != "1/1/2000 12:00:00 AM" && theloai != null && quocgia != null)
            {
                loaiPhepLoc = 1;
                listP.ItemsSource = new PhimBUS().locPhimTheoTatCaDieuKien(rap.MaRap, ngaychieu, theloai.MaTheLoai, quocgia.MaQuocGia);
                return;
            }
            //tim theo quoc gia, the loai
            if(rap != null && theloai != null)
            {
                loaiPhepLoc = 2;
                listP.ItemsSource = new PhimBUS().locPhimTheoRap_TheLoai(rap.MaRap, theloai.MaTheLoai, ngaychieu);
                return;
            }
            if (rap != null && quocgia != null)
            {
                loaiPhepLoc = 3;
                listP.ItemsSource = new PhimBUS().locPhimTheoRap_QG(rap.MaRap,quocgia.MaQuocGia, ngaychieu);
                return;
            }
            if (quocgia != null && theloai != null)
            {
                loaiPhepLoc = 4;
                listP.ItemsSource = new PhimBUS().locPhimTheoQG_TL(quocgia.MaQuocGia, theloai.MaTheLoai);
                return;
            }
            if (rap != null)
            {
                loaiPhepLoc = 5;
                listP.ItemsSource = new PhimBUS().locPhimTheoRap(rap.MaRap, ngaychieu);
                return;
            }
            if (theloai != null)
            {
                loaiPhepLoc = 6;
                listP.ItemsSource = new PhimBUS().locPhimTheoTheLoai(theloai.MaTheLoai);
                return;
            }
            if(quocgia != null)
            {
                loaiPhepLoc = 7;
                listP.ItemsSource = new PhimBUS().locPhimTheoQuocGia(quocgia.MaQuocGia);
            }
        }

        private void btnLamMoi_Click(object sender, RoutedEventArgs e)
        {
            loaiPhepLoc = 0;
            InitializeComponent();
            Switcher.pageSwitcher = mainWindow;
            Switcher.Switch(new TimKiemPhim(mainWindow));
        }

        private void txtTenPhim_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Return)
            {
                InitializeComponent();
                if (loaiPhepLoc == 0)
                {
                    if (txtTenPhim.Text == "")
                    {
                        listP.ItemsSource = new PhimBUS().layToanBoPhim();
                    }
                    else
                    {
                        listP.ItemsSource = new PhimBUS().timKiemPhimTheoTen(txtTenPhim.Text);
                    }
                }
                Rap rap = null;
                if (cmbRap.SelectedItem != null)
                {
                    rap = cmbRap.SelectedItem as Rap;
                }
                DateTime ngaychieu = new DateTime(2000, 1, 1);
                if (cmbNgayChieu.SelectedItem != null)
                {
                    ngaychieu = (DateTime)cmbNgayChieu.SelectedItem;
                }
                TheLoai theloai = null;
                if (cmbTheLoai.SelectedItem != null)
                {
                    theloai = cmbTheLoai.SelectedItem as TheLoai;
                }
                QuocGia quocgia = null;
                if (cmbQuocGia.SelectedItem != null)
                {
                    quocgia = cmbQuocGia.SelectedItem as QuocGia;
                }
                if (loaiPhepLoc == 1)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen1(txtTenPhim.Text, rap.MaRap, quocgia.MaQuocGia, theloai.MaTheLoai, ngaychieu);
                    listP.ItemsSource = ls;
                }
                if (loaiPhepLoc == 2)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen2(txtTenPhim.Text, rap.MaRap, ngaychieu, theloai.MaTheLoai);
                    listP.ItemsSource = ls;
                }
                if (loaiPhepLoc == 3)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen3(txtTenPhim.Text, rap.MaRap, ngaychieu, quocgia.MaQuocGia);
                    listP.ItemsSource = ls;
                }
                if (loaiPhepLoc == 4)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen4(txtTenPhim.Text, quocgia.MaQuocGia, theloai.MaTheLoai);
                    listP.ItemsSource = ls;
                }
                if (loaiPhepLoc == 5)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen5(txtTenPhim.Text, rap.MaRap, ngaychieu);
                    listP.ItemsSource = ls;
                }
                if (loaiPhepLoc == 6)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen6(txtTenPhim.Text, theloai.MaTheLoai);
                    listP.ItemsSource = ls.Where(p => p.TheLoai == theloai.MaTheLoai).ToList();
                }
                if (loaiPhepLoc == 7)
                {
                    var ls = new PhimBUS().timKiemPhimTheoTen7(txtTenPhim.Text, quocgia.MaQuocGia);
                    listP.ItemsSource = ls;
                }
            }
        }

        private void Trailer_Click(object sender, RoutedEventArgs e)
        {

        }

        private void DatVe_Click(object sender, RoutedEventArgs e)
        {
            Switcher.pageSwitcher = mainWindow;
            Switcher.Switch(new DanhSachRap(mainWindow, "201705060511", "000000000000"));
        }
    }
}
