﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.ThanhVien
{
    class ThongTinThanhVien
    {
        public String MaThanhVien { get; set; }
        public String TenThanhVien { get; set; }
        public String SoDienThoai { get; set; }
        public DateTime NgaySinh { get; set; }
        public String CMND { get; set; }
        public int DiemThuong { get; set; }
        public int SoVeDoiThuong { get; set; }
        public String LoaiKhachHang { get; set; }
    }
}
