﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.ThanhVien
{
    /// <summary>
    /// Interaction logic for XemVeDat.xaml
    /// </summary>
    /// 
    public partial class XemVeDat : UserControl
    {
        public static MainWindow mainWindow;
        public string MaNguoiDung;
        public int STT = 1;
        public XemVeDat(MainWindow windows, string _maNguoiDung)
        {
            mainWindow = windows;
            MaNguoiDung = _maNguoiDung;
            InitializeComponent();
            var ls = new VeXemPhimBUS().layVeDat(_maNguoiDung);
            string ghe = "";
            
            
            VeXemPhim v = ls.First();
            var list = ls.Where(vp => vp.MaSuatChieu == v.MaSuatChieu).ToList();
            foreach (var item in list)
            {
                ghe += item.Ghe.DayGhe + item.Ghe.SoTT + ", ";
            }
            ghe = ghe.Substring(0, ghe.Length - 2);
            txtGhe.Text = ghe;
            this.DataContext = v;
            Pre.Visibility = Visibility.Hidden;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            STT = STT + 1;
            Pre.Visibility = Visibility.Visible;
            var ls = new VeXemPhimBUS().layVeDat(MaNguoiDung);
            string ghe = "";


            VeXemPhim v = ls[STT -1];
            var list = ls.Where(vp => vp.MaSuatChieu == v.MaSuatChieu).ToList();
            foreach (var item in list)
            {
                ghe += item.Ghe.DayGhe + item.Ghe.SoTT + ", ";
                
            }
            ghe = ghe.Substring(0, ghe.Length - 2);
            txtGhe.Text = ghe;
            this.DataContext = v;
            if(STT == ls.Count())
            {
                Next.Visibility = Visibility.Hidden;
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            STT = STT - 1;
            Next.Visibility = Visibility.Visible;
            var ls = new VeXemPhimBUS().layVeDat(MaNguoiDung);
            string ghe = "";


            VeXemPhim v = ls[STT -1];
            var list = ls.Where(vp => vp.MaSuatChieu == v.MaSuatChieu).ToList();
            foreach (var item in list)
            {
                ghe += item.Ghe.DayGhe + item.Ghe.SoTT + ", ";

            }
            ghe = ghe.Substring(0, ghe.Length - 2);
            txtGhe.Text = ghe;
            this.DataContext = v;
            if (STT == 0)
            {
                Pre.Visibility = Visibility.Hidden;
            }
        }
    }
}
