﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.ThanhVien
{
    class Phim
    {
        public String MaPhim { get; set; }
        public String TenPhim { get; set; }
        public DateTime NgayCongChieu { get; set; }
        public String LinkHinhAnh { get; set; }
        public String LinkTrailler { get; set; }
        public int ThoiLuong { get; set; }
        public String MoTa { get; set; }
        public int DanhGia { get; set; }
        public int TinhTrang { get; set; }
        public String DaoDien { get; set; }

    }
}
