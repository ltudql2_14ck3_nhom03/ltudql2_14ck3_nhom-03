﻿using mycinema.BUS;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace mycinema.ThanhVien
{
    /// <summary>
    /// Interaction logic for DoiMatKhau.xaml
    /// </summary>
    public partial class DoiMatKhau : Window
    {
        public string MaND;
        public DoiMatKhau(string _maNguoiDung)
        {
            MaND = _maNguoiDung;
            WindowStartupLocation = System.Windows.WindowStartupLocation.CenterScreen;
            InitializeComponent();
        }

        private void btnDoiMatKhau_Click(object sender, RoutedEventArgs e)
        {
            if(String.IsNullOrWhiteSpace(txtMKHienTai.Password) || String.IsNullOrWhiteSpace(txtMKMoi.Password) || String.IsNullOrWhiteSpace(txtMKNL.Password))
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Vui lòng nhập đầy đủ mật khẩu";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
                return;
            }
            if(txtMKMoi.Password != txtMKNL.Password)
            {
                var messageQueue = Snackbar.MessageQueue;
                string message = "Mật khẩu nhập lại không khớp";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
                return;
            }
            bool dungMatKhau = new NguoiDungBUS().NhapDungMatKhau(MaND.ToString(), txtMKHienTai.Password);

            if(dungMatKhau == true && txtMKMoi.Password == txtMKNL.Password)
            {
                if(MessageBox.Show("Bạn có thật sự muốn đổi mật khẩu?", "Xác nhận", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
                {
                    bool kq = new NguoiDungBUS().DoiMatKhau(MaND.ToString(), txtMKMoi.Password);
                    if(kq)
                    {
                        var messageQueue = Snackbar.MessageQueue;
                        string message = "Đổi mật khẩu thành công";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);
                        MessageBox.Show("Đổi mật khẩu thành công");
                        this.Close();
                    }
                    else
                    {
                        var messageQueue = Snackbar.MessageQueue;
                        string message = "Đổi mật khẩu thất bại";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);
                        return;
                    }
                }
            }
        }
    }
}
