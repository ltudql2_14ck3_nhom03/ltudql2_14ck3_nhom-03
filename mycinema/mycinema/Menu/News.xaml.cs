﻿using mycinema.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for News.xaml
    /// </summary>
    public partial class News : UserControl
    {
        public News()
        {
            InitializeComponent();
            this.ListNews.ItemsSource = new TinTuc[]
         {
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")},
                new TinTuc{Title="News 1", ImageData=LoadImage("conan.jpg")}
         };
        }
        private BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri("pack://application:,,,/Resources/img/" + filename));
        }
    }
}
