﻿using MaterialDesignThemes.Wpf;
using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu.MenuBar
{
    /// <summary>
    /// Interaction logic for CinemaManagerMenuBar.xaml
    /// </summary>
    public partial class CinemaManagerMenuBar : UserControl
    {
        private CinemaManager cinemaManager;
        public CinemaManagerMenuBar(CinemaManager cinemaManager)
        {
            InitializeComponent();
            this.cinemaManager = cinemaManager;
            cbbAddNewOwner.DataContext = (new QuanTriVienBUS()).layDSQuanTriVien();
        }
        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("SAMPLE 1: Closing dialog with parameter: " + (eventArgs.Parameter ?? ""));

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

        }

        private void btnAddNew_Click(object sender, RoutedEventArgs e)
        {
            string id = tbAddNewId.Text;
            string name = tbAddNewName.Text;
            string address = tbAddNewAddress.Text;
            string status = ((ComboBoxItem)cbbAddNewStatus.SelectedItem).Content.ToString();
            string photo = ipAddNewPhoto.ImgPath;
            string owner = cbbAddNewOwner.SelectedValue.ToString();
            Rap rap = new Rap() { MaRap = id, TenRap = name, DiaChi = address, TinhTrang = status/*, AnhRap = photo*/, NguoiTao = owner};
            RapBUS rBUS = new RapBUS();
            rBUS.themRap(rap);
            cinemaManager.hienThiDSRap();
        }
    }
}
