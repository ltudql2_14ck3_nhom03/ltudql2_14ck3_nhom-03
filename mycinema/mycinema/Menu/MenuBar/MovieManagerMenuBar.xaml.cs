﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu.MenuBar
{
    /// <summary>
    /// Interaction logic for MovieManagerMenuBar.xaml
    /// </summary>
    public partial class MovieManagerMenuBar : UserControl
    {
        private string cinemarName;
        private ManagerFrame managerFrame;
        public MovieManagerMenuBar(ManagerFrame managerFrame)
        {
            InitializeComponent();
            this.managerFrame = managerFrame;
        }

        public string CinemarName
        {
            get
            {
                return cinemarName;
            }

            set
            {
                cinemarName = value;
                btnTenRap.Content = cinemarName;
            }
        }

        private void btnTenRap_Click(object sender, RoutedEventArgs e)
        {
            managerFrame.setViewContent(ManagerFrame.CINEMA_MANAGER_FLAG);
        }
    }
}
