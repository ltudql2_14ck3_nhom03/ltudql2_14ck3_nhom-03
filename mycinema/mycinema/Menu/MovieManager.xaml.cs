﻿using MaterialDesignThemes.Wpf;
using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for MovieManager.xaml
    /// </summary>
    public partial class MovieManager : UserControl
    {
        private string cinemaId;
        public MovieManager()
        {
            InitializeComponent();
            


        }
        private void loadInstance()
        {
            RapBUS dsRapBUS = new RapBUS();
            List<Phim> listPhim = dsRapBUS.layDanhPhimCuaRap(cinemaId);
            lvMovie.ItemsSource = listPhim;
        }

        public string CinemaId
        {
            get
            {
                return cinemaId;
            }

            set
            {
                cinemaId = value;
                loadInstance();
            }
        }
        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("SAMPLE 1: Closing dialog with parameter: " + (eventArgs.Parameter ?? ""));

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

        }
    }
}
