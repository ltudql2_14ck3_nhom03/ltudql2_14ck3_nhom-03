﻿using mycinema.Menu.MenuBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for ManagerFrame.xaml
    /// </summary>
    public partial class ManagerFrame : UserControl
    {
        public  const int CINEMA_MANAGER_FLAG = 1;
        public  const  int MOVIE_MANAGER_FLAG = 2;
        public const int MEMBER_MANAGER_FLAG = 3;

        public static MainWindow mainWindow;

        private CinemaManager cinemaManager;
        private CinemaManagerMenuBar cinemaManagerMenuBar;

        private MovieManager movieManager;
        private MovieManagerMenuBar movieManagerMenuBar;


        private MemberManager memberManager;

        public ManagerFrame(MainWindow windows)
        {
            mainWindow = windows;
            InitializeComponent();

            // set instance panels
            cinemaManager = new CinemaManager(this);
            cinemaManagerMenuBar = new CinemaManagerMenuBar(cinemaManager);

            movieManager = new MovieManager();
            movieManagerMenuBar = new MovieManagerMenuBar(this);

            memberManager =  new MemberManager();

            // set default panel
            setViewContent(CINEMA_MANAGER_FLAG);
        }

        private void btnCinemaManager_Click(object sender, RoutedEventArgs e)
        {
            setViewContent(CINEMA_MANAGER_FLAG);
        }

        private void btnMemberManager_Click(object sender, RoutedEventArgs e)
        {
            setViewContent(MEMBER_MANAGER_FLAG);
        }
        private void setViewContent(UserControl mainContent, UserControl menuBarContent)
        {
            ccContent.Content = mainContent;
            ctcMenuBar.Content = menuBarContent;
        }
        public void setViewContent(int viewType, params object[] param)
        {
            
            switch (viewType)
            {
                case CINEMA_MANAGER_FLAG:
                    setViewContent(cinemaManager, cinemaManagerMenuBar);
                    break;
                case MOVIE_MANAGER_FLAG:
                    movieManager.CinemaId = (string)param[0];
                    movieManagerMenuBar.CinemarName = (string)param[1];
                    setViewContent(movieManager, movieManagerMenuBar);
                    break;
                case MEMBER_MANAGER_FLAG:
                    setViewContent(memberManager, null);
                    break;
            }
        }
    }
}
