﻿using mycinema.Booking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for Home.xaml
    /// </summary>
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
            this.ListFilm.ItemsSource = new Phim[]
          {
                new Phim{Name="Conan 1",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 2",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 3",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 4",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 5",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 6",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 7",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 8",Price=200, ImageData=LoadImage("conan.jpg")},
                new Phim{Name="Conan 9",Price=200, ImageData=LoadImage("conan.jpg")}
          };
        }
        private BitmapImage LoadImage(string filename)
        {
            return new BitmapImage(new Uri("pack://application:,,,/Resources/img/" + filename));
        }
    }
}
