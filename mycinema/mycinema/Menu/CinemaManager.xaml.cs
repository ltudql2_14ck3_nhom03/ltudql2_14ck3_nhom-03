﻿using MaterialDesignThemes.Wpf;
using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for CinemaManager.xaml
    /// </summary>
    public partial class CinemaManager : UserControl
    {
        private ManagerFrame managerFrame;
        public CinemaManager(ManagerFrame managerFrame)
        {
            InitializeComponent();
            this.managerFrame = managerFrame;
            hienThiDSRap();
	}

        public void hienThiDSRap()
        {
            QuanTriVienBUS qtvBUS = new QuanTriVienBUS();
            RapBUS rapBUS = new RapBUS();
            List<Rap> listRap = rapBUS.layDanhSachToanBoRap();
            List<QuanTriVien> lstQTV = qtvBUS.layDSQuanTriVien();
            // set items source
            lvCinema.ItemsSource = listRap;
            
        }

        private void btnMovieManager_Click(object sender, RoutedEventArgs e)
        {
            Rap dataSender = (Rap)((Button)sender).DataContext;
            string maRap = dataSender.MaRap;
            string tenRap = dataSender.TenRap;
            managerFrame.setViewContent(ManagerFrame.MOVIE_MANAGER_FLAG, maRap, tenRap);
        }
        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("SAMPLE 1: Closing dialog with parameter: " + (eventArgs.Parameter ?? ""));

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

        }

        private void btnCapNhatPhong_Click(object sender, RoutedEventArgs e)
        {
            
        }
    }
}
