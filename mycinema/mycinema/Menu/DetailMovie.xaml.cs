﻿using MaterialDesignThemes.Wpf;
using mycinema.Booking;
using mycinema.BUS;
using mycinema.Models;
using mycinema.ViewModels;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for DetailMovie.xaml
    /// </summary>
    public partial class DetailMovie : UserControl
    {
        private string IDLogin;
        private Models.Phim phim;

        private DanhGiaBUS danhGiaBUS = new DanhGiaBUS();
        PhimBUS phimBUS = new PhimBUS();
        SuatChieuBUS scBUS = new SuatChieuBUS();
        public DetailMovie()
        {
            InitializeComponent();

            // temp value
            IDLogin = "000000000001";

            phim = phimBUS.layPhimTheoMaPhim("201705060511");
            this.DataContext = phim;
            List<DienVien> lstDienVien = phim.DienViens.ToList();
            lvCastImage.DataContext = lstDienVien;

            // init data context
            // suat chieu data context
            List<DanhSachNgayChieu> lstSuat = scBUS.layLichChieu(phim.SuatChieux.ElementAt(0).PhongChieu.MaRap, phim.MaPhim);
            lstContainer.DataContext = new ObservableCollection<DanhSachNgayChieu>(lstSuat);


            // binh luan data context
            List<BinhLuan> lstBinhLuan = phim.BinhLuans.ToList();
            lvComment.DataContext = lstBinhLuan;

            // rating data context
            updateRatingValue();

            // your rating
            int yourRating = 0;
            yourRating = danhGiaBUS.laySoSaoCuaNguoiDanhGia(IDLogin, phim.MaPhim);
            rtbYourRating.Value = yourRating;

            // trailer source
            //meTrailer.DataContext = "https://redirector.googlevideo.com/videoplayback?initcwndbps=2070000&key=yt6&requiressl=yes&lmt=1493070498516100&sparams=dur%2Cei%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Cratebypass%2Crequiressl%2Csource%2Cupn%2Cexpire&ipbits=0&dur=183.576&mime=video%2Fmp4&itag=22&upn=qc5cpW25hto&mn=sn-a5m7znes&mm=31&expire=1495284199&ratebypass=yes&source=youtube&ip=2001%3A19f0%3A7001%3Ad32%3A5400%3Aff%3Afe58%3A19e7&ms=au&ei=h-UfWePfHcW84gKMiproCA&id=o-ACBNrX0m1eStN1kltDvL2s4NWXVoa4OKd50fob8iR99Q&pl=38&mv=m&mt=1495262479&signature=54A4860F40C903CACAE3AF225E84E7E8D9815314.E2067F9D9BDCFC5AAEB6E7D7B18A4491C0FD958D";



        }

        private void updateRatingValue()
        {
            DuLieuDanhGiaPhim dlDanhGiaPhim = danhGiaBUS.layDuLieuDanhGiaPhim(phim.MaPhim);
            tbRating.DataContext = new DanhGia{ SoSao = dlDanhGiaPhim.SoSao };
            rtbRating.DataContext = new DanhGia { SoSao = dlDanhGiaPhim.SoSao };
            tbSoNguoiDanhGia.DataContext = new { SoNguoiDanhGia = dlDanhGiaPhim.SoNguoiDanhGia};
        }
        
        
        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("SAMPLE 1: Closing dialog with parameter: " + (eventArgs.Parameter ?? ""));

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;
            
        }

        private void rtbYourRating_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            int soSao;
            bool checkParse = int.TryParse(rtbYourRating.Value.ToString(), out soSao);

            if (checkParse && !string.IsNullOrWhiteSpace(IDLogin) && !string.IsNullOrWhiteSpace(phim.MaPhim))
            {
                DanhGia danhGia = new DanhGia() { MaNguoiDung = IDLogin, MaPhim = phim.MaPhim, SoSao = soSao };
                DanhGiaBUS danhGiaBUS = new DanhGiaBUS();
                danhGiaBUS.DanhGia(danhGia);
                updateRatingValue();
            }

        }
    }
}
