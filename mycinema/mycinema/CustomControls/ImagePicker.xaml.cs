﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.CustomControls
{
    /// <summary>
    /// Interaction logic for ImagePicker.xaml
    /// </summary>
    public partial class ImagePicker : UserControl
    {
        private string imgPath;
        public ImagePicker()
        {
            InitializeComponent();
            imgPath = "";
        }

        public string ImgPath
        {
            get
            {
                return imgPath;
            }

            set
            {
                imgPath = value;
            }
        }

        private void btnBrowse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            Nullable<bool> result = ofd.ShowDialog();
            if (result == true)
            {
                imgPath = ofd.FileName;
                imgPreview.Source = new BitmapImage(new Uri(ofd.FileName));
            }
        }
    }
}
