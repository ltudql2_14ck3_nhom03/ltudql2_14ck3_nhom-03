﻿using mycinema.Booking;
using mycinema.Menu;
using mycinema.ThanhVien;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            Switcher.pageSwitcher = this;
            //Switcher.Switch(new DanhSachRap(this));
            //Switcher.Switch(new LoginSignUpPanel(this));
            frame.Navigate(new TimKiemPhim(this));
            //Switcher.pageSwitcher = this;
            //Switcher.Switch(new DanhSachRap(this, "201705060511", "000000000001"));
            //Switcher.Switch(new DetailMovie(this));
        }

        public void Navigate(UserControl nextPage)
        {
            this.Content = nextPage;
        }
    }
}
