﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.ObjectDataProvider
{
    class QuanTriVienDP
    {
        List<QuanTriVien> lstQuanTriVien;

        public QuanTriVienDP(List<QuanTriVien> lstQuanTriVien)
        {
            this.lstQuanTriVien = lstQuanTriVien;
        }
        public QuanTriVienDP()
        {
            
        }

        public List<QuanTriVien> LstQuanTriVien
        {
            get
            {
                return lstQuanTriVien;
            }

            set
            {
                lstQuanTriVien = value;
            }
        }
        public List<QuanTriVien> getLst()
        {
            lstQuanTriVien = (new QuanTriVienBUS()).layDSQuanTriVien();
            return lstQuanTriVien;
        }
    }
}
