﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;




namespace mycinema.Booking
{
    /// <summary>
    /// Interaction logic for DanhSachRap.xaml
    /// </summary>
    public partial class DanhSachRap : UserControl
    {
        public static MainWindow mainWindow;

        private string maPhim { get; set; }
        private string maRap { get; set; }

        private List<Rap> listRap;

        private string maKH { get; set; }
             
        public DanhSachRap(MainWindow windows, string maP, string mkh)
        {
            mainWindow = windows;
            InitializeComponent();
            maPhim = maP;
            maKH = mkh;
            RapBUS dsrBUS = new RapBUS();
            listRap = dsrBUS.layDanhSachToanBoRap();
            

           

            this.DataContext = listRap;
        }

        
       

        private void btnPickRap_Click(object sender, RoutedEventArgs e)
        {


            Rap r = (Rap)((sender as Button).DataContext);
 

           
            
            Switcher.pageSwitcher = mainWindow;
            
            //maPhim = "201705060511";
            
            DanhSachSuatChieu dsscWindows = new DanhSachSuatChieu(mainWindow, r, maPhim, maKH, null);
            Switcher.Switch(dsscWindows);
            
        }
    }
}
