﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Booking
{
    /// <summary>
    /// Interaction logic for ManHinhXacNhan.xaml
    /// </summary>
    public partial class ManHinhXacNhan : UserControl
    {
        public static MainWindow mainWindow;

        private Rap rap { get; set; }
        private List<VeXemPhim> lstVeP { get; set; }
        public ManHinhXacNhan(MainWindow windows, List<VeXemPhim> lstVXP)
        {
            mainWindow = windows;
            InitializeComponent();
            if(lstVXP != null)
            {
                lstVeP = lstVXP;
                RapBUS rBUS = new RapBUS();
                PhimBUS pBUS = new PhimBUS();
                SuatChieuBUS scBUS = new SuatChieuBUS();
                GheBUS gBUS = new GheBUS();
                PhongBUS phBUS = new PhongBUS();

                string masc = lstVeP.ElementAt(0).MaSuatChieu;
                SuatChieu sc = scBUS.layThongTinSuatChieu(masc);
                
                rap = rBUS.layThongTinRap(sc);
                
                mycinema.Models.Phim phim = pBUS.layThongTinBoPhim(sc);
                

                List<ThongTinThanhToan> lstTTTT = new List<ThongTinThanhToan>();
                foreach(VeXemPhim vxp in lstVeP)
                {
                    Ghe g = gBUS.LayThongTinGhe(vxp);
                    PhongChieu pc = phBUS.layThongTinPhong(g);

                    ThongTinThanhToan tttt = new ThongTinThanhToan()
                    {
                        ve = vxp,
                        tenRap = rap.TenRap,
                        tenPhim = phim.TenPhim,
                        soTTGhe = g.SoTT,
                        dayGhe = g.DayGhe,
                        tgBD = sc.ThoiGianBD,
                        ngayChieu = sc.NgayChieu,
                        soPhong = pc.STT
                        
                        
                    };
                    lstTTTT.Add(tttt);

                }

                lsViewVe.DataContext = lstTTTT;
                configMenu();
            }
            
        }

        private void configMenu()
        {
            menuPhu.menuCon.txtRap.FontWeight = FontWeights.Regular;
            menuPhu.menuCon.btnChonSuat.Background = Brushes.Red;
            menuPhu.menuCon.btnChonSuat.BorderBrush = Brushes.Red;
            menuPhu.menuCon.btnChonGhe.Background = Brushes.Red;
            menuPhu.menuCon.btnChonGhe.BorderBrush = Brushes.Red;
            menuPhu.menuCon.btnThanhToan.Background = Brushes.Red;
            menuPhu.menuCon.btnThanhToan.BorderBrush = Brushes.Red;
            menuPhu.menuCon.btnXacNhan.Background = Brushes.Red;
            menuPhu.menuCon.btnXacNhan.BorderBrush = Brushes.Red;
            menuPhu.menuCon.txtXacNhan.FontWeight = FontWeights.Bold;
            menuPhu.txtNext.Visibility = Visibility.Hidden;
            menuPhu.txtPrev.Visibility = Visibility.Hidden;
            menuPhu.btnNext.Visibility = Visibility.Hidden;
            menuPhu.btnPrev.Visibility = Visibility.Hidden;
            menuPhu.menuCon.lineGhe.Background = Brushes.Red;
            menuPhu.menuCon.lineSuat.Background = Brushes.Red;
            menuPhu.menuCon.lineThanhToan.Background = Brushes.Red;

            menuPhu.txtTenHeader.Text = "XÁC NHẬN";
            menuPhu.txtTenRap.Text = rap.TenRap;
            menuPhu.txtTenRap.Typography.Capitals = FontCapitals.AllSmallCaps;
        }

    }
}
