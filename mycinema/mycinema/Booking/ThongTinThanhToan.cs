﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.Booking
{
    public class ThongTinThanhToan
    {
        public VeXemPhim ve { get; set; }
        public string tenRap { get; set; }
        public string tenPhim { get; set; }
        public int? soTTGhe { get; set; }
        public string dayGhe { get; set; }

        public TimeSpan? tgBD { get; set;}
        public DateTime? ngayChieu { get; set; }

        public int? soPhong { get; set; }
    }
}
