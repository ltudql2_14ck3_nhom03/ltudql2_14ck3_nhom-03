﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Booking
{
    /// <summary>
    /// Interaction logic for ManHinhThanhToan.xaml
    /// </summary>
    public partial class ManHinhThanhToan : UserControl
    {

        public static MainWindow mainWindow;

        private SuatChieu sc { get; set; }
        private string maRap { get; set; }

        private List<Ghe> lstGhe { get; set; }

        private long tongTien { get; set; }

        private string maKH { get; set; }
        private NguoiDung nd { get; set; }

        private int soVeDoi { get; set; }

        private bool suDungKM { get; set; }
        private List<VeXemPhim> lstVe { get; set; }
        
        public ManHinhThanhToan(MainWindow windows, SuatChieu x, string mkh, List<Ghe> lsg)
        {
            mainWindow = windows;
            InitializeComponent();

            lstVe = new List<VeXemPhim>();
            maKH = mkh;
            sc = x;
            lstGhe = lsg;
            tongTien = lsg.Count * 60000;
            

            NguoiDungBUS ndBUS = new NguoiDungBUS();
            nd = ndBUS.layThongTinNguoiDung(maKH); 

            configMenuPhu();
            
            
        }

        private void configMenuPhu()
        {
            menuPhu.menuCon.txtRap.FontWeight = FontWeights.Regular;
            menuPhu.menuCon.btnThanhToan.Background = Brushes.Red;
            menuPhu.menuCon.btnThanhToan.BorderBrush = Brushes.Red;
            menuPhu.menuCon.lineThanhToan.Background = Brushes.Red;
            menuPhu.menuCon.txtThanhToan.FontWeight = FontWeights.Bold;
            menuPhu.menuCon.btnChonSuat.Background = Brushes.Red;
            menuPhu.menuCon.btnChonSuat.BorderBrush = Brushes.Red;
            menuPhu.menuCon.btnChonGhe.Background = Brushes.Red;
            menuPhu.menuCon.btnChonGhe.BorderBrush = Brushes.Red;
            menuPhu.menuCon.lineSuat.Background = Brushes.Red;
            menuPhu.menuCon.lineGhe.Background = Brushes.Red;
            menuPhu.txtNext.Text = "XÁC NHẬN";
            menuPhu.txtPrev.Text = "CHỌN GHẾ";
            

            menuPhu.infoContainer.Visibility = Visibility.Hidden;
            menuPhu.detailContainer.Visibility = Visibility.Visible;
            menuPhu.btnNext.Visibility = Visibility.Hidden;
            menuPhu.txtNext.Visibility = Visibility.Hidden;

            var stackDetail = menuPhu.detailContainer as StackPanel;
            (stackDetail.FindName("txtTenPhim") as TextBlock).Text = sc.Phim.TenPhim;
            (stackDetail.FindName("txtTenPhim") as TextBlock).Text.ToUpper();
            (stackDetail.FindName("txtTenPhim") as TextBlock).FontWeight = FontWeights.Bold;
            (stackDetail.FindName("txtThoiGian") as TextBlock).Text = Convert.ToDateTime(sc.NgayChieu).ToShortDateString().ToString() + " " + sc.ThoiGianBD;
            (stackDetail.FindName("txtSolg") as TextBlock).Text = "Số lượng: " + (lstGhe.Count()) + " vé";
            (stackDetail.FindName("txtTongTien") as TextBlock).Text = "Tổng tiền: " + String.Format("{0:C}", tongTien);

            menuPhu.btnPrev.Click += btnPrev_Click;

            btnTheNganHang.Background = Brushes.Red;
            btnTheNganHang.BorderBrush = Brushes.Red;
            btnTheNganHang.Foreground = Brushes.White;

            containThanhToanThe.Visibility = Visibility.Visible;
            containThanhToanTrucTiep.Visibility = Visibility.Collapsed;

            btnThanhToanTrucTiep.BorderBrush = Brushes.White;
            btnThanhToanTrucTiep.Background = Brushes.White;
            btnThanhToanTrucTiep.Foreground = Brushes.Black;

            
            tbHoTen.DataContext = nd;
            tbSoDT.DataContext = nd;
            tbSoCMND.DataContext = nd;

            ThongTinTheModel ttThe = new ThongTinTheModel();
            tbSoTK.DataContext = ttThe;
            tbNgHH.DataContext = ttThe;
            tbSoCVV.DataContext = ttThe;

            txtDiemTichLuy.Text = (int)nd.KhachHang.DiemThuong + "";
            List<int> lstSolg = new List<int>();
            soVeDoi = (int)nd.KhachHang.DiemThuong / 100;

            for(int i = 0; i < soVeDoi + 1; i++)
            {
                lstSolg.Add(i);
            }

            cbbVeThuong.ItemsSource = lstSolg;
            cbbVeThuong.SelectedIndex = 0;
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            Switcher.pageSwitcher = mainWindow;
            DanhSachGhe dsg = new DanhSachGhe(mainWindow, sc, maKH, null);
            Switcher.Switch(dsg);
        }



        private void btnTheNganHang_Click(object sender, RoutedEventArgs e)
        {

            ((Button)sender).Background = Brushes.Red;
            ((Button)sender).BorderBrush = Brushes.Red;
            ((Button)sender).Foreground = Brushes.White;

            containThanhToanThe.Visibility = Visibility.Visible;
            containThanhToanTrucTiep.Visibility = Visibility.Collapsed;

            btnThanhToanTrucTiep.BorderBrush = Brushes.White;
            btnThanhToanTrucTiep.Background = Brushes.White;
            btnThanhToanTrucTiep.Foreground = Brushes.Black;
        }

        private void btnThanhToanTrucTiep_Click(object sender, RoutedEventArgs e)
        {
            ((Button)sender).Background = Brushes.Red;
            ((Button)sender).BorderBrush = Brushes.Red;
            ((Button)sender).Foreground = Brushes.White;

            containThanhToanThe.Visibility = Visibility.Collapsed;
            containThanhToanTrucTiep.Visibility = Visibility.Visible;

            btnTheNganHang.BorderBrush = Brushes.White;
            btnTheNganHang.Background = Brushes.White;
            btnTheNganHang.Foreground = Brushes.Black;
        }

        private void cbbVeThuong_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int value = (int)this.cbbVeThuong.SelectedItem;
            long tienGiamGia = tongTien;
            int soVeMua = lstGhe.Count();

            if (value > soVeMua)
            {

                cbbVeThuong.SelectedIndex = soVeMua;

                var messageQueue = Snackbar.MessageQueue;
                var message = "Số lượng vé đổi thưởng vượt quá số lượng vé cần mua";
                Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);
            }
            else
            {
                tienGiamGia -= value * 60000;
                var stackDetail = menuPhu.detailContainer as StackPanel;
                (stackDetail.FindName("txtTongTien") as TextBlock).Text = "Tổng tiền: " + String.Format("{0:C}", tienGiamGia);
                soVeMua-= value;
            }
            if(value == 0)
            {
                suDungKM = false;
            }
            else
            {
                suDungKM = true;
            }
        }

        private void btnThanhToan_Click(object sender, RoutedEventArgs e)
        {
            
            if(containThanhToanThe.Visibility == Visibility.Visible)
            {
                if(!String.IsNullOrWhiteSpace(tbSoTK.Text) && !Validation.GetHasError(tbSoTK)
                    && !String.IsNullOrWhiteSpace(tbNgHH.Text) && !Validation.GetHasError(tbNgHH)
                    && !String.IsNullOrWhiteSpace(tbSoCVV.Text) && !Validation.GetHasError(tbSoCVV))
                {
                    //luu ve
                    luuVe();
                    denMHXacNhan();
                }
                else
                {
                    var messageQueue = Snackbar.MessageQueue;
                    string message = "Vui lòng nhập đầy đủ thông tin để tiến hành thanh toán";
                    Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
                }
            }
            else
            {
                if(!String.IsNullOrWhiteSpace(tbHoTen.Text) && !Validation.GetHasError(tbHoTen)
                    && !String.IsNullOrWhiteSpace(tbSoDT.Text) && !Validation.GetHasError(tbSoDT)
                    && !String.IsNullOrWhiteSpace(tbSoCMND.Text) && !Validation.GetHasError(tbSoCMND))
                {
                    //luu ve
                    luuVe();
                    denMHXacNhan();
                }
                else
                {
                    var messageQueue = Snackbar.MessageQueue;
                    string message  = "Vui lòng nhập đầy đủ thông tin để tiến hành thanh toán";
                    Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
                }
            }
        }

        private void denMHXacNhan()
        {
            Switcher.pageSwitcher = mainWindow;
            ManHinhXacNhan mhxn = new ManHinhXacNhan(mainWindow, lstVe);
            Switcher.Switch(mhxn);
        }
        

        private void luuVe()
        {
            var messageQueue = Snackbar.MessageQueue;
            string message = null;
            VeXemPhimBUS vxpBUS = new VeXemPhimBUS();
            foreach (Ghe ghe in lstGhe)
            {
                vxpBUS.luuThongTinVe(sc.MaSuatChieu, ghe.MaGhe, maKH, lstVe);
                Thread.Sleep(1000);
            }

            if (!suDungKM)
            {
                NguoiDungBUS ngdBUS = new NguoiDungBUS();
                tongTien = lstGhe.Count() * 60000;
                ngdBUS.capNhatThongTinNguoiDung(maKH, tongTien);
               
            }

            
            message = "Thanh toán thành công";
            Snackbar.MessageQueue.Enqueue(
            message,
            "OK",
            param => Trace.WriteLine("Actioned: " + param),
            message);

        }
    }
}
