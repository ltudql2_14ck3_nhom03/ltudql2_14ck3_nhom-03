﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.Booking
{
    public class DanhSachThongTinGhe
    {
        public ObservableCollection<ThongTinGhe> lstGhe { get; set; }

        public ObservableCollection<string> lstSoHang { get; set; }
        public ObservableCollection<int?> lstSoCot { get; set; }
    }
}
