﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace mycinema.Booking
{
    public class Rule : ValidationRule
    {

        public string txt { get; set; }

        public Rule(string str)
        {
            this.txt = str;
        }

        public Rule()
        {

        }
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            switch (txt)
            {
                case "soTK":
                    string stk = (string)value;
                    if(!stk.All(Char.IsNumber) || stk.Any(Char.IsWhiteSpace))
                    {
                        return new ValidationResult(false, "Số tài khoản không chính xác");
                    }
                    if(stk.Count() != 16)
                    {
                        return new ValidationResult(false, "Số tài khoản phải đủ 16 chữ số");
                    }
                    break;
                case "ngayHetHan":
                    string nghh = (string)value;
                    if(nghh.Any(Char.IsLetter) || nghh.IndexOf('/') != 2 || nghh.Count() != 5)
                    {
                        return new ValidationResult(false, "Định dạng ngày không đúng");
                    }
                    break;
                case "socvv":
                    string scvv = (string)value;
                    if(!scvv.All(Char.IsNumber) || scvv.Count() != 4)
                    {
                        return new ValidationResult(false, "Số CVV không chính xác");
                    }
                    break;
                case "hoten":
                    string ht = (string)value;
                    if(ht.Count() == 0 || ht.All(Char.IsWhiteSpace))
                    {
                        return new ValidationResult(false, "Họ tên không được rỗng");
                    }
                    break;
                case "diachi":
                    string dc = (string)value;
                    if(dc.Count() == 0)
                    {
                        return new ValidationResult(false, "Địa chỉ không được rỗng");
                    }
                    break;
                case "sdt":
                    string sdt = (string)value;
                    if (!sdt.All(Char.IsNumber))
                    {
                        return new ValidationResult(false, "Số điện thoại không chính xác");
                    }
                    break;
                case "socmnd":
                    string cmnd = (string)value;
                    if (!cmnd.All(Char.IsNumber))
                    {
                        return new ValidationResult(false, "Số CMND không chính xác");
                    }
                    break;
                
            }
            return new ValidationResult(true, null);
        }
    }
}
