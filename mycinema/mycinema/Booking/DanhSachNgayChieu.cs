﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.Booking
{
    public class DanhSachNgayChieu
    {
        public DateTime ngay { get; set; }
        public ObservableCollection<SuatChieu> lstSuat { get; set; }
    }
}
