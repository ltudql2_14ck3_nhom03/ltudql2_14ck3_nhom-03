﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Booking
{
    /// <summary>
    /// Interaction logic for DanhSachSuatChieu.xaml
    /// </summary>
    public partial class DanhSachSuatChieu : UserControl
    {
        public static MainWindow mainWindow;
        private string maPhim { get; set; }
        private Rap rap { get; set; }
        private SuatChieu sc { get; set; }

        private string maKH { get; set; }



        public DanhSachSuatChieu(MainWindow windows, Rap r, string maP, string mkh, SuatChieu x)
        {
            mainWindow = windows;
            InitializeComponent();
            rap = r;
            maPhim = maP;
            maKH = mkh;
            

            menuPhu.btnPrev.Click += btnNextRap_Click;
            if(x == null)
            {
                menuPhu.btnNext.Visibility = Visibility.Hidden;
                menuPhu.txtNext.Visibility = Visibility.Hidden;
            }
            else
            {
                sc = x;
                menuPhu.btnNext.Visibility = Visibility.Visible;
                menuPhu.btnNext.Visibility = Visibility.Visible;
                menuPhu.btnNext.Click += btnNextGhe_Click;
            }

            configMenuPhu(1);

            SuatChieuBUS scBUS = new SuatChieuBUS();
            List<DanhSachNgayChieu> lstSuat = scBUS.layLichChieu(rap.MaRap, maPhim);

            
            this.DataContext = new ObservableCollection<DanhSachNgayChieu>(lstSuat);

        }

        private void configMenuPhu(int option)
        {
            if (option == 1)
            {
                menuPhu.menuCon.btnChonSuat.Background = Brushes.Red;
                menuPhu.menuCon.btnChonSuat.BorderBrush = Brushes.Red;
                menuPhu.menuCon.txtSuat.FontWeight = FontWeights.Bold;
                menuPhu.menuCon.txtRap.FontWeight = FontWeights.Regular;
                menuPhu.menuCon.lineSuat.Background = Brushes.Red;
                menuPhu.txtTenRap.Text = rap.TenRap;
                menuPhu.txtTenRap.Text.ToUpper();
            }
            if(option == 0)
            {
                menuPhu.menuCon.btnChonSuat.Background = Brushes.White;
                menuPhu.menuCon.btnChonSuat.BorderBrush = Brushes.White;
                menuPhu.menuCon.txtSuat.FontWeight = FontWeights.Regular;
                menuPhu.menuCon.txtRap.FontWeight = FontWeights.Bold;
                menuPhu.menuCon.lineSuat.Background = Brushes.White;
            }
            
        }

        private void btnNextRap_Click(object sender, RoutedEventArgs e)
        {
    
            Switcher.pageSwitcher = mainWindow;

            DanhSachRap dsr = new DanhSachRap(mainWindow, maPhim, maKH);
            Switcher.Switch(dsr);

        }

        private void btnNextGhe_Click(object sender, RoutedEventArgs e)
        {
            Switcher.pageSwitcher = mainWindow;
            if(sc != null)
            {
                DanhSachGhe dsg = new DanhSachGhe(mainWindow, sc, maKH, null);
                Switcher.Switch(dsg);
            }
        }

        private void btnDatSuat_Click(object sender, RoutedEventArgs e)
        {

            var dataContext = ((Button)sender).DataContext;
            sc = (SuatChieu)dataContext;
            DanhSachGhe dsg = new DanhSachGhe(mainWindow, sc, maKH, null);
            Switcher.Switch(dsg);

           
      
        }

       
    }

   
}
