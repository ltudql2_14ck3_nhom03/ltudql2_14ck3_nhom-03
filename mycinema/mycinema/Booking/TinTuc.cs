﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace mycinema.Booking
{
    public class TinTuc
    {
        private string _title;
        public string Title
        {
            get { return this._title; }
            set { this._title = value; }
        }


        private BitmapImage _ImageData;
        public BitmapImage ImageData
        {
            get { return this._ImageData; }
            set { this._ImageData = value; }
        }
    }
}
