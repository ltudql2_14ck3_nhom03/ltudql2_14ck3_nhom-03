﻿using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Globalization;
using System.Diagnostics;

namespace mycinema.Booking
{
    /// <summary>
    /// Interaction logic for DanhSachGhe.xaml
    /// </summary>
    public partial class DanhSachGhe : UserControl
    {
        public static MainWindow mainWindow;

        private SuatChieu sc { get; set; }
        private string maRap { get; set; }

        private List<Ghe> lstGhe { get; set; }
        private string maKH { get; set; }

        private List<Ghe> lstGheDaDat { get; set; }
        public DanhSachGhe(MainWindow windows,SuatChieu x, string mkh, List<Ghe> lsg)
        {
            mainWindow = windows;
            InitializeComponent();
            
            maKH = mkh;
            string maPhong = x.MaPhong;
            sc = x;
            configMenu();

            GheBUS gheBUS = new GheBUS();
            DanhSachThongTinGhe ds = gheBUS.layDanhSachGhe(sc);

            lstGheDaDat = gheBUS.layCacGheDaDat(sc.MaPhong, sc.MaSuatChieu);

            List<DanhSachThongTinGhe> dstt = new List<DanhSachThongTinGhe>();
            dstt.Add(ds);

            this.DataContext = new ObservableCollection<DanhSachThongTinGhe>(dstt);

            if (lsg == null)
            {
                lstGhe = new List<Ghe>();
                menuPhu.btnNext.Visibility = Visibility.Hidden;
                menuPhu.txtNext.Visibility = Visibility.Hidden;



            }
            else
            {
                lstGhe = lsg;
            }

            var messageQueue = Snackbar.MessageQueue;
            var message = "Vui lòng chọn ghế bạn muốn đặt";
            Snackbar.MessageQueue.Enqueue(
                message,
                "OK",
                param => Trace.WriteLine("Actioned: " + param),
                message);


        }

        

        private void configMenu()
        {

            menuPhu.txtPrev.Text = "CHỌN SUẤT";
            menuPhu.txtNext.Text = "THANH TOÁN";

            menuPhu.menuCon.btnChonSuat.Background = Brushes.Red;
            menuPhu.menuCon.btnChonSuat.BorderBrush = Brushes.Red;
            menuPhu.menuCon.txtRap.FontWeight = FontWeights.Regular;
            menuPhu.menuCon.lineSuat.Background = Brushes.Red;
            var stackDetail = menuPhu.detailContainer as StackPanel;
            RapBUS rBUS = new RapBUS();
            (stackDetail.FindName("txtTenRap") as TextBlock).Text = rBUS.layThongTinRap(sc).TenRap;
            (stackDetail.FindName("txtTenRap") as TextBlock).Text.ToUpper();
            menuPhu.txtTenHeader.Text = "DANH SÁCH GHẾ";

            menuPhu.menuCon.btnChonGhe.Background = Brushes.Red;
            menuPhu.menuCon.txtGhe.FontWeight = FontWeights.Bold;
            menuPhu.menuCon.lineGhe.Background = Brushes.Red;
            menuPhu.menuCon.btnChonGhe.BorderBrush = Brushes.Red;

            menuPhu.btnPrev.Click += btnPrev_Click;
            
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            
            Switcher.pageSwitcher = mainWindow;
            SuatChieuBUS scBUS = new SuatChieuBUS();
            Rap r = scBUS.layThongTinRap(sc);

            DanhSachSuatChieu dsr = new DanhSachSuatChieu(mainWindow, r, sc.MaPhim, maKH, sc);
            Switcher.Switch(dsr);

        }

        
        

        private void btnGhe_Click_1(object sender, RoutedEventArgs e)
        {
            var gheChon = (ThongTinGhe)((Button)sender).DataContext;
            ThongTinGhe ttGhe = gheChon;
            Ghe g = ttGhe.ghe;

            if (lstGheDaDat.Contains(g))
            {
                var messageQueue = Snackbar.MessageQueue;
                var message = "Vị trí ghế đã có người đặt. Vui lòng chọn ghế khác";
                Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
            }
            else
            {
                var btn = (Button)sender;
               
                var messageQueue = Snackbar.MessageQueue;
                var message = "Bạn chọn ghế: ";

                

                if (!lstGhe.Contains(g))
                {
                    lstGhe.Add(g);
                    btn.Background = Brushes.HotPink;
                    btn.BorderBrush = Brushes.HotPink;
                }
                else
                {
                    lstGhe.Remove(g);
                    btn.Background = Brushes.GreenYellow;
                    btn.BorderBrush = Brushes.GreenYellow;
                }

                txtSlg.Text = lstGhe.Count() + "";

                foreach (Ghe ghe in lstGhe)
                {

                    message += ghe.DayGhe + "" + ghe.SoTT;
                    message += " ";
                }
                Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
            }
        }

        private void btnThanhToan_Click(object sender, RoutedEventArgs e)
        {
            GheBUS gBus = new GheBUS();
            List<Ghe> dsGheDat = gBus.layCacGheDaDat(sc.MaPhong, sc.MaSuatChieu);
            foreach(Ghe g in lstGhe)
            {
                if (dsGheDat.Contains(g))
                {
                    var messageQueue = Snackbar.MessageQueue;
                    var message = "Một trong các ghế của bạn vừa chọn đã được người khác đặt. Vui lòng thử lại ";
                    Snackbar.MessageQueue.Enqueue(
                    message,
                    "OK",
                    param => Trace.WriteLine("Actioned: " + param),
                    message);
                }
                else
                {
                    //den man hinh thanh toan
                    Switcher.pageSwitcher = mainWindow;
                    ManHinhThanhToan mhtt = new ManHinhThanhToan(mainWindow, sc, maKH, lstGhe);
                    Switcher.Switch(mhtt);

                }
            }

        }
    }


    public class ColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value != null)
            {
                int type = Int32.Parse(value.ToString());
                if(type == 1)
                {
                    return new SolidColorBrush(Colors.Red);

                }
                if (type == 0)
                {
                    return new SolidColorBrush(Colors.GreenYellow);
                }
            }
            return null;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new Exception("Method is not implemented");
        }
    }
}
