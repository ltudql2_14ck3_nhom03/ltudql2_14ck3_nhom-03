﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for LoginSignUpPanel.xaml
    /// </summary>
    public partial class LoginSignUpPanel : UserControl
    {
        public LoginSignUpPanel()
        {
            InitializeComponent();
            frame.Navigate(new FormLogin());
        }
        private void listChoose_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = listChoose.SelectedIndex;
            if (index == 0)
            {
                try
                {
                    frame.Navigate(new FormLogin());
                }
                catch 
                {
                }
            }
            if (index == 1)
            {
                try
                {
                    frame.Navigate(new FormSignup());

                }
                catch
                {
                }
            }
        }
    }
}
