﻿using MaterialDesignThemes.Wpf;
using mycinema.BUS;
using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.Menu
{
    /// <summary>
    /// Interaction logic for MemberManager.xaml
    /// </summary>
    public partial class MemberManager : UserControl
    {
        private NguoiDungBUS ngdBUS = new NguoiDungBUS();
        private ManagerFrame managerFrame;
        public MemberManager()
        {
            InitializeComponent();

            
            List<NguoiDung> dsNgD = ngdBUS.layDanhSachNguoiDung();
            dgThanhVien.DataContext = dsNgD;

        }

        private void btnXoa_Click(object sender, RoutedEventArgs e)
        {
            if(dgThanhVien.SelectedItem != null)
            {
                if(dgThanhVien.SelectedItem is NguoiDung)
                {
                    NguoiDung nd = dgThanhVien.SelectedItem as NguoiDung;
                    if(ngdBUS.capNhatTrangThaiNguoiDung(nd) > 0)
                    {
                        var messageQueue = Snackbar.MessageQueue;
                        var message = "Cập nhật trạng thái thành công";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);

                        dgThanhVien.DataContext = ngdBUS.layDanhSachNguoiDung();
                    }
                    else
                    {
                        var messageQueue = Snackbar.MessageQueue;
                        var message = "Cập nhật trạng thái thất bại";
                        Snackbar.MessageQueue.Enqueue(
                        message,
                        "OK",
                        param => Trace.WriteLine("Actioned: " + param),
                        message);
                    }
                }
            }
        }

        private void btnThemTV_Click(object sender, RoutedEventArgs e)
        {

        }

        private void Sample1_DialogHost_OnDialogClosing(object sender, DialogClosingEventArgs eventArgs)
        {
            Console.WriteLine("SAMPLE 1: Closing dialog with parameter: " + (eventArgs.Parameter ?? ""));

            //you can cancel the dialog close:
            //eventArgs.Cancel();

            if (!Equals(eventArgs.Parameter, true)) return;

        }
    }
}
