﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class QuocGiaBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public List<QuocGia> layToanBoQuocGia()
        {
            return db.QuocGias.ToList();
        }
    }
}
