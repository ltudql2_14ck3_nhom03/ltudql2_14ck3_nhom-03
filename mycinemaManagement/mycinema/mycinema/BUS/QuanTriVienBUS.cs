﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class QuanTriVienBUS
    {
        private const string ACTIVE_STATUS = "Kích hoạt";
        private const string DISABLE_STATUS = "Khóa";
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public List<QuanTriVien> layDSQuanTriVien()
        {
            List<QuanTriVien> rst = db.QuanTriViens.Where(q => q.NguoiDung.
            TinhTrang.CompareTo(ACTIVE_STATUS) == 0).ToList<QuanTriVien>();
            return rst;
        }
    }
}
