﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class PhimBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public List<Phim> timKiemPhimTheoTen(string _tenphim)
        {
            List < Phim > ls = db.Phims.Where(p => p.TenPhim == _tenphim).ToList<Phim>();
            return ls;
        }
        public List<Phim> layToanBoPhim()
        {
            return db.Phims.OrderByDescending(p => p.NgayCongChieu).ToList<Phim>();
        }

        public List<Phim> locPhimTheoTatCaDieuKien(string _maRap, DateTime _ngayChieu, string _maTheLoai, string _maQuocGia)
        {
            return db.Phims.Where(
                p => p.QuocGia1.MaQuocGia == _maQuocGia).Where(
                p => p.TheLoai1.MaTheLoai == _maTheLoai).Where(
                p => p.SuatChieux.FirstOrDefault().PhongChieu.MaRap == _maRap).ToList<Phim>();
        }
        public List<Phim> locPhimTheoRap_TheLoai(string _maRap, string _maTheLoai)
        {
            return db.Phims.Where(p => p.TheLoai == _maTheLoai).Where(p => p.SuatChieux.FirstOrDefault().PhongChieu.MaRap == _maRap).ToList<Phim>();
        }
        public List<Phim> locPhimTheoRap_QG(string _maRap, string _maQuocGia)
        {
            return db.Phims.Where(p => p.QuocGia == _maQuocGia).Where(p => p.SuatChieux.FirstOrDefault().PhongChieu.MaRap == _maRap).ToList<Phim>();
        }
        public List<Phim> locPhimTheoQG_TL(string _maQuocGia, string _maTheLoai)
        {
            return db.Phims.Where(p => p.QuocGia == _maQuocGia).Where(p => p.TheLoai == _maTheLoai).ToList<Phim>();
        }
        public List<Phim> locPhimTheoRap(string _maRap)
        {
            return db.Phims.Where(p => p.SuatChieux.FirstOrDefault().PhongChieu.MaRap == _maRap).ToList();
        }
        public List<Phim> locPhimTheoTheLoai(string _theLoai)
        {
            return db.Phims.Where(p => p.TheLoai1.MaTheLoai == _theLoai).ToList();
        }
        public List<Phim> locPhimTheoQuocGia(string _maQuocGia)
        {
            return db.Phims.Where(p => p.QuocGia1.MaQuocGia == _maQuocGia).ToList<Phim>();
        }
    }
}
