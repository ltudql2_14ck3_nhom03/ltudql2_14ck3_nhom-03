﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class NguoiDungBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public NguoiDung layThongTinNguoiDung(string ma)
        {
            NguoiDung nd = db.NguoiDungs.Where(u => u.MaNguoiDung.Equals(ma)).Select(u => u).First();
            return nd;
        }


        public int capNhatTrangThaiNguoiDung(NguoiDung nd)
        {
            if(db.NguoiDungs.Where(u => u.MaNguoiDung.Equals(nd.MaNguoiDung) && u.TinhTrang.Equals("active")).Count() > 0)
            {
                nd.TinhTrang = "deactive";
                return db.SaveChanges();
            }
            return -1;
        }

        public List<NguoiDung> layDanhSachNguoiDung()
        {
            return db.NguoiDungs.Select(u => u).ToList<NguoiDung>();
        }

        public void capNhatThongTinNguoiDung(string makh, long tongTien) {
            KhachHang kh = db.KhachHangs.Where(c => c.MaKhachHang.Equals(makh)).Select(c => c).First();
            var value = db.KhachHangs.Find(makh);
            if(value != null)
            {
                var diemCu = value.DiemThuong;
                value.DiemThuong = (int)(tongTien / 60000) * 100 + diemCu;


                try
                {
                    db.SaveChanges();

                }catch(Exception ex)
                {
                    //return 0;
                }
            }

            
        }
    }
}
