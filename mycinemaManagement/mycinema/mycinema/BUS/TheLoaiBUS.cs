﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    class TheLoaiBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public List<TheLoai> layToanBoTheLoai()
        {
            return db.TheLoais.ToList();
        }
    }
}
