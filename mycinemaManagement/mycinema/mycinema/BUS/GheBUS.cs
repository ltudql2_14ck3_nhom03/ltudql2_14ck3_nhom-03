﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class GheBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();

        public List<Ghe> layToanBoGhe(string maPhong)
        {

    
            
            List<Ghe> lstGhe = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g).ToList<Ghe>();
            return lstGhe;
        }

        public List<Ghe> layCacGheDaDat(string maPhong,string maSuat)
        {
            List<Ghe> lstGheDaDat = db.Ghes.Join(db.VeXemPhims, g => g.MaGhe, v => v.MaGhe, (g, v) => new { g = g, v = v })
                .Where(i => (i.g.MaPhong.Equals(maPhong)) && (i.v.MaSuatChieu.Equals(maSuat))).Select(i => i.g).ToList<Ghe>();
            return lstGheDaDat;
        }

        public List<String> laySoHangGhe(string maPhong)
        {
            List<String> lstHang = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g.DayGhe).Distinct().ToList<String>();
            return lstHang;
        }

        public List<int?> laySoCotGhe(string maPhong)
        {
            List<int?> lstCot = db.Ghes.Where(g => g.MaPhong.Equals(maPhong)).Select(g => g.SoTT).Distinct().ToList<int?>();
            return lstCot;
        }

        
    }
}
