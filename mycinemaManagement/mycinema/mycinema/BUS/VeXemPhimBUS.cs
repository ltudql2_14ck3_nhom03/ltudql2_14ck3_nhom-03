﻿using mycinema.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mycinema.BUS
{
    public class VeXemPhimBUS
    {
        private QLRapChieuEntities db = new QLRapChieuEntities();
        public void luuThongTinVe(string maSc, string maGhe, string mangd)
        {
            DateTime date = DateTime.Now;
            string year = date.Year.ToString();
            string month = date.Month.ToString();
            string day = date.Day.ToString();
            string hour = date.Hour.ToString();
            string second = date.Second.ToString();
            string maVe = year + month + day + hour + second;
            VeXemPhim vxp = new VeXemPhim()
            {
                MaVe = maVe,
                MaSuatChieu = maSc,
                MaGhe = maGhe,
                MaNguoiDung = mangd,
                ThoiGianDat = date,
                GiaVe = 60000
            };

            db.VeXemPhims.Add(vxp);

            try
            {
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                //return 0;
            }

            
        }
        public double? layTongDoanhThuThang(int _nam,int _thang)
        {
            var dt = db.VeXemPhims.Where(v => v.SuatChieu.NgayChieu.Value.Year == _nam).Where(v => v.SuatChieu.NgayChieu.Value.Month == _thang).Sum(v => v.GiaVe);
            if(dt == null)
            {
                return 0;
            }
            return (double)dt;
        }
        public double? layDoanhThuRapTheoThang(int _nam,int _thang, string _maRap)
        {
            var ls = db.VeXemPhims.Where(v => v.Ghe.PhongChieu.MaRap == _maRap).Where(v => v.SuatChieu.NgayChieu.Value.Year == _nam).Where(v => v.SuatChieu.NgayChieu.Value.Month == _thang).Sum(v => v.GiaVe);
            if(ls == null)
            {
                return 0;
            }
            return ls;
        }

        public double? layDoanhThuRapTheoNam(int _nam, string _maRap)
        {
            var dt = db.VeXemPhims.Where(v => v.Ghe.PhongChieu.MaRap == _maRap).Where(v => v.SuatChieu.NgayChieu.Value.Year == _nam).Sum(v => v.GiaVe);
            if(dt == null)
            {
                return 0;
            }
            return dt;
        }
        public List<int> layNam()
        {
            var dsNam = db.VeXemPhims.OrderByDescending(v => v.SuatChieu.NgayChieu.Value.Year).Select(v => v.SuatChieu.NgayChieu.Value.Year).Distinct().ToList();
            return dsNam;
        }
    }
}
