﻿using mycinema.BUS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace mycinema.ThongKe
{
    /// <summary>
    /// Interaction logic for ThongKeDoanhSo.xaml
    /// </summary>
    public partial class ThongKeDoanhSo : UserControl
    {
        public ThongKeDoanhSo()
        {
            InitializeComponent();
            var nam = new VeXemPhimBUS().layNam();
            cmbBieuDoThang.ItemsSource = nam;
            cmbBieuDoTron.ItemsSource = nam;
            if(nam != null)
            {
                int i = 0;
                foreach(var item in cmbBieuDoThang.Items)
                {
                    if((int)item == DateTime.Today.Year)
                    {
                        cmbBieuDoThang.SelectedIndex = i;
                        cmbBieuDoTron.SelectedIndex = i;
                    }
                    i++;
                }
            }
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            int thang = DateTime.Today.Month;
            if (thang == 1)
            {
                btn1.Background = Brushes.Red;
            }
            if (thang == 2)
            {
                btn2.Background = Brushes.Red;
            }
            if (thang == 3)
            {
                btn3.Background = Brushes.Red;
            }
            if (thang == 4)
            {
                btn4.Background = Brushes.Red;
            }
            if (thang == 5)
            {
                btn5.Background = Brushes.Red;
            }
            if (thang == 6)
            {
                btn6.Background = Brushes.Red;
            }
            if (thang == 7)
            {
                btn7.Background = Brushes.Red;
            }
            if (thang == 8)
            {
                btn8.Background = Brushes.Red;
            }
            if (thang == 9)
            {
                btn9.Background = Brushes.Red;
            }
            if (thang == 10)
            {
                btn10.Background = Brushes.Red;
            }
            if (thang == 11)
            {
                btn11.Background = Brushes.Red;
            }
            if (thang == 12)
            {
                btn12.Background = Brushes.Red;
            }
            showColumnChart((int)cmbBieuDoThang.SelectedValue, DateTime.Today.Month);
            showPieChart((int)cmbBieuDoThang.SelectedValue);
        }
        private void showColumnChart(int nam, int thang)
        {
            List<KeyValuePair<string, int>> valueList = new List<KeyValuePair<string, int>>();
            
            var lsRap = new RapBUS().layDanhSachToanBoRap();
            double? tongdoanhthuthang = new VeXemPhimBUS().layTongDoanhThuThang(nam,thang);
            valueList.Add(new KeyValuePair<string, int>("Tổng cộng", (int)tongdoanhthuthang));
            foreach (var item in lsRap)
            {
                double? doanhthu = new VeXemPhimBUS().layDoanhThuRapTheoThang(nam,thang, item.MaRap);
                valueList.Add(new KeyValuePair<string, int>(item.TenRap, (int)doanhthu));
            }
            //Setting data for column chart
            columnChart.DataContext = valueList;

        }
        private void showPieChart(int nam)
        {
            List<KeyValuePair<string, int>> valueList1 = new List<KeyValuePair<string, int>>();
            var lsRap = new RapBUS().layDanhSachToanBoRap();
            foreach (var item in lsRap)
            {
                double? doanhthunam = new VeXemPhimBUS().layDoanhThuRapTheoNam(nam, item.MaRap);
                valueList1.Add(new KeyValuePair<string, int>(item.TenRap, (int)doanhthunam));
            }
            // Setting data for pie chart
            pieChart.DataContext = valueList1;
        }
        private void cmbBieuDoThang_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 1);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn1.Background = Brushes.Red;
        }

        private void btn1_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 1);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn1.Background = Brushes.Red;
        }

        private void btn2_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 2);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = Brushes.Red;
        }

        private void btn3_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 3);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = Brushes.Red;
        }

        private void btn4_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 4);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = Brushes.Red;
        }

        private void btn5_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 5);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = Brushes.Red;
        }

        private void btn6_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 6);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = Brushes.Red;
        }

        private void btn7_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 7);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = Brushes.Red;
        }

        private void btn8_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 8);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = Brushes.Red;
        }

        private void btn9_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 9);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = Brushes.Red;
        }

        private void btn10_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 10);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = Brushes.Red;
        }

        private void btn11_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 11); BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = Brushes.Red;
        }

        private void btn12_Click(object sender, RoutedEventArgs e)
        {
            showColumnChart((int)cmbBieuDoThang.SelectedValue, 12);
            BrushConverter bc = new BrushConverter();
            btn1.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn2.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn3.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn4.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn5.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn6.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn7.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn8.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn9.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn10.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn11.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = (Brush)bc.ConvertFrom("#FF673AB7");
            btn12.Background = Brushes.Red;
        }

        private void cmbBieuDoTron_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            showPieChart((int)cmbBieuDoThang.SelectedValue);
        }
    }
}
