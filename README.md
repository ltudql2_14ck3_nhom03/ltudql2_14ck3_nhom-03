# README #
# ỨNG DỤNG QUẢN LÝ RẠP CHIẾU PHIM #
## Đồ án lập trình ứng dụng quản lý 2. Nhóm 3 Lớp 14CK3 ##

## SCREENSHOT ##

# USER #

![19688490_1306339816146155_461829242_o.png](https://bitbucket.org/repo/9pKB5gz/images/2702365543-19688490_1306339816146155_461829242_o.png)

![19619903_1306340176146119_405737035_o.png](https://bitbucket.org/repo/9pKB5gz/images/1056391190-19619903_1306340176146119_405737035_o.png)

![19650129_1306341536145983_59444961_o.png](https://bitbucket.org/repo/9pKB5gz/images/168019337-19650129_1306341536145983_59444961_o.png)

![19688512_1306340756146061_640067158_o.png](https://bitbucket.org/repo/9pKB5gz/images/4282733041-19688512_1306340756146061_640067158_o.png)


![19650285_1306340882812715_1714700041_o.png](https://bitbucket.org/repo/9pKB5gz/images/2340887489-19650285_1306340882812715_1714700041_o.png)

![19647947_1306340549479415_1350637069_o.png](https://bitbucket.org/repo/9pKB5gz/images/2571034997-19647947_1306340549479415_1350637069_o.png)

# ADMIN #
![19619919_1306341932812610_1116627928_o.png](https://bitbucket.org/repo/9pKB5gz/images/1520895021-19619919_1306341932812610_1116627928_o.png)

![19688429_1306342282812575_230336057_o.png](https://bitbucket.org/repo/9pKB5gz/images/3139066333-19688429_1306342282812575_230336057_o.png)

![19650126_1306342499479220_961274845_o.png](https://bitbucket.org/repo/9pKB5gz/images/1766131565-19650126_1306342499479220_961274845_o.png)

![19648367_1306342132812590_859084851_o.png](https://bitbucket.org/repo/9pKB5gz/images/2192963303-19648367_1306342132812590_859084851_o.png)

![19648213_1306342626145874_488254450_o.png](https://bitbucket.org/repo/9pKB5gz/images/1686744097-19648213_1306342626145874_488254450_o.png)

# LẤY CONTROL TEMPLATE TẠI ĐÂY: #
- DOWNLOAD SOURCE VỀ R CHỌN LỌC COPY WPF CODE VÀO PROJECT
[https://github.com/ButchersBoy/MaterialDesignInXamlToolkit](Link URL)
*LƯU Ý NHỚ SỬ DỤNG USER CONTROL CHO 1 MÀN HÌNH NHƯ DEMO ĐÃ LÀM*

Thực hành cài đặt ứng dụng quản lý rạp chiếu phim trên ngôn ngữ C# + WPF

** Tao User control **
![Capture.PNG](https://bitbucket.org/repo/9pKB5gz/images/3046023889-Capture.PNG)

**Quan trong!!: Checkout branch Develop ve:**

```
#!c#

git remote add origin https://1461576@bitbucket.org/ltudql2_14ck3_nhom03/ltudql2_14ck3_nhom-03.git
git fetch && git checkout Develop
```

### Quy trình nghiệp vụ ###
![Nhom3-1461576-1461458-1461350-1461641.png](https://bitbucket.org/repo/9pKB5gz/images/4098205079-Nhom3-1461576-1461458-1461350-1461641.png)

### Mô tả ###
* 1	PHÁT BIỂU BÀI TOÁN
* a.	Bối cảnh:
* -	Nhu cầu đặt vé nhanh, gọn của khách hàng hiện nay và đỡ tốn thời gian khi đi mua vé xem phim, với hình thức thanh toán trực tiếp qua thẻ sẽ rất thuận lời cho người tiêu dùng, ứng dụng này được phát triển nhằm những mục đích đó. 
* b.	Đặc điểm:
* -	Ứng dụng nhằm tin học hóa việc tra cứu phim ảnh chiếu trên các cụm rạp, xem thông tin về phim, và đặt vé xem phim giúp giảm các thao tác rườm rà và mất thời gian khi mua vé trực tiếp.
* c.	Nội dung:
* -	Về đối tượng tin học hóa:
* +	Quản lý danh sách các phim (phim đang công chiếu, sắp công chiếu,..).
* +	Quản lý vé bán.
* +	Quản lý thành viên (Có 3 loại thành viên: Silver Member, Gold Member, Premium Member).
* +	Quản lý các phòng chiếu.
* +	Các cụm rạp trên cùng hệ thống rạp chiếu.
* -	Về quy trình nghiệp vụ:
* +	Admin:
* 	Sau khi đăng nhập với tư cách là quản trị viên thì có thể thực hiện các chức năng ở những mục bên dưới.
* 	Hệ quản trị có thể quản lý danh sách các phim.
* 	Thống kê số vé đã bán cho từng loại phim.
* 	Quản lý và phân bổ các suất chiếu.
* 	Quản lý các phòng chiếu (Về tình trạng phòng, phòng đang hoạt động,..)
* 	Quản lý thành viên của hệ thống.
* 	Quản lý các cụm rạp (địa điểm mỗi cụm rạp là khác nhau).
* +	Thành viên:
* •	Quy trình tìm kiếm và đặt vé xem phim:
* 	Người dùng khi vào ứng dụng được phép xem thông tin các phim đang công chiếu trên mạng, các phim sắp chiếu hoặc có thể tìm, lọc đến phim mong muốn đặt vé.
* 	Khi tìm kiếm phim, người dùng được phép tìm đến tên phim trực tiếp hoặc sử dụng chức năng lọc phim của hệ thông như: địa điểm rạp chiếu, tên cụm rạp, tên phim, ngày công chiếu.
* 	Sau khi chuyển đến phim mong muốn, ở đây thành viên được phép xem thông tin về phim ảnh như mô tả bộ phim, rating IMDb, trailer, và có thể đặt vé trực tiếp cho phim tại đây.
* 	Khách hàng cần đăng nhập vào hệ thống với tư cách là thành viên của hệ thống mới có thể tiếp tục thực hiện chức năng đặt vé phim.
* 	Nếu chưa có thông tin về cụm rạp, tên phim và ngày chiếu thì khách hàng sẽ được nhập lại thông tin đó, sau đó khách hàng được phép chọn vị trí ghế ngồi trong rạp.
* 	Khách hàng cung cấp thông tin như: mã số thẻ, năm hết hạn, tên in trên thẻ,.. các thông tin cá nhân khác được lấy từ tài khoản mà thành viên đăng nhập để được thanh toán.
* 	Đặt vé xem phim thông qua ứng dụng (Ở mức độ đồ án sẽ không làm online) nên sẽ không có chức năng xác thực thông qua email hoặc sđt, chia sẽ,..
* 	Thành viên sau khi đặt một bộ phim sẽ được tích 1 điểm thưởng:
* o	Đối với Silver member có thể đổi điểm sang cấp hạng (Tích được 15 điểm sẽ được lên GM, 25 điểm được lên PM).
* o	Bất cứ thành viên nào nếu tích được 10 điểm sẽ được đổi 1 vé xem phim.
* o	Chính sách ưu đãi ở:
* 	 Premium member: được đặt vé ưu tiên (Số vé dự trù cho 1 suất chiếu là 10 vé). Được miễn phí vẽ xem vào dịp sinh nhật.
* 	Gold member: được miễn phí vé xem phim vào dịp sinh nhật.
* •	Những quy trình nhỏ lẽ khác:
* 	Thành viên sau đăng nhập có thể xem thông tin tài khoản như: mã số tài khoản, tên, điểm thưởng.
* 	Chức năng xem review về một bộ phim.
* 	Xem đoạn trailer của một bộ film.
* 	Bình luận một bộ phim.
* 2	CHỨC NĂNG PHẦN MỀM
* -	Phía người dùng (Front-end):
* STT	Mô tả
* FF01	Hiển thị danh sách các phim đang công chiếu, sắp công chiếu, phim đang hot.
* FF02	Đăng nhập vào hệ thống.
* FF03	Xem thông tin thành viên (Điểm tích được, loại hạng, thông tin cá nhân, mã số thẻ).
* FF04	Đặt vé xem phim.
* FF05	Tìm kiếm phim theo tên.
* FF06	Lọc phim theo địa điểm cụm rạp, thời gian và phim.
* FF07	Hiển thị chi tiêt thông tin một bộ phim (tên, trailer, suất chiểu, rating IMBd,..).
* FF08	Đánh giá một bộ phim.
* FF09	Bình Luận một bộ phim.
* FF10	Xem review một bộ phim.
* FF11	Thăng cấp hạng.
* FF12 	Đổi vé xem film.
* FF13	Đăng ký.
* FF14	Đổi mật khẩu
* 
* -	Phía người quản trị (Back-end):
* STT	Mô tả
* BF01	QL các thành viên (Tìm kiếm, xem chi tiết, hùy tài khoản, tăng hạng thành viên, đổi thưởng).
* BF02	QL các suất chiếu cho 1 cho một phim (xem chi tiết, sắp xếp chỉnh sửa,..)
* BF03	QL phòng chiếu phim (quản lý về tình trạng, các suât chiểu trong ngày, loại phim công chiếu)
* BF04	Thống kê số vé bán được trong chu kì 4 tháng, doanh thu, số thành viên.
* BF05	Quản lý cụm rạp.
* BF06	Quản lý danh sách phim (Thêm, chỉnh sửa thông tin đăng tải)
* BF07	Đăng tải các bài review về phim ảnh

### Phân công ###
* **Ho Tuan Sang:** 
* FF07: Hiển thị chi tiêt thông tin một bộ phim (tên, trailer, suất chiểu, rating IMBd,..).
* FF08: Đánh giá một bộ phim.
* FF09: Bình Luận một bộ phim.
* BF02: QL các suất chiếu cho 1 cho một phim (xem chi tiết, sắp xếp chỉnh sửa,..)
* BF03: QL phòng chiếu phim (quản lý về tình trạng, các suât chiểu trong ngày, loại phim công chiếu)
* BF05: Quản lý cụm rạp.
* BF06: Quản lý danh sách phim (Thêm, chỉnh sửa thông tin đăng tải)
* **Do Nguyen Minh Luan:** 
* FF04: Đặt vé xem phim.
*  	FF10: Xem review một bộ phim.
* BF01: QL các thành viên (Tìm kiếm, xem chi tiết, hùy tài khoản, tăng hạng thành viên, đổi thưởng).
* FF11: Thăng cấp hạng. (Chưc năng này trong màn hình quản lý thành viên)
* **Duong Gia Hoa:** 
* FF02: Đăng nhập vào hệ thống.
* FF03:  Xem thông tin thành viên (Điểm tích được, loại hạng, thông tin cá nhân, mã số thẻ).
* FF12:  Đổi vé xem film.
* FF13:  Đăng ký.
* FF14: Đổi mật khẩu
* BF04: Thống kê số vé bán được trong chu kì 4 tháng, doanh thu, số thành viên.
* **Ngo Ngoc Thien:** 
* FF01: Hiển thị danh sách các phim đang công chiếu, sắp công chiếu, phim đang hot.
* FF05: Tìm kiếm phim theo tên.
* FF06: Lọc phim theo địa điểm cụm rạp, thời gian và phim.
* BF07: Đăng tải các bài review về phim ảnh
*